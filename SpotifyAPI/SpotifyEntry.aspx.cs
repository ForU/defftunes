﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

namespace SpotifyAPI
{
    public partial class SpotifyEntry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AuthorizeButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=user-read-private&state=83jfdkd9dlslvx3w&redirect_uri=http://localhost:64270/Callback.aspx&show_dialog=true");
        }
    }
}