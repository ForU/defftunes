﻿<%@ Page Title="" Language="C#" MasterPageFile="~/views/defftunes.Master" AutoEventWireup="true" CodeBehind="SpotifyEntry.aspx.cs" Inherits="SpotifyAPI.views.SpotifyEntry" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>
<asp:content id="content2" contentplaceholderid="maincontent" runat="server">
    <br />
    <%--<div class="container">--%>
        <form id="mainForm" class=""  runat="server">
            <div class="form-group">
                <%--<asp:Button runat="server" type="button" class="btn btn-success" onclick="AuthorizeButton_Click" text="Authorize"></asp:Button>--%>

                <%--<asp:CheckBoxList id="scopeList" class="scopeList checkbox"
                   AutoPostBack="True"
                   CellPadding="5"
                   CellSpacing="5"
                   RepeatColumns="2"
                   RepeatDirection="Vertical"
                   RepeatLayout="Flow"
                   TextAlign="Right"
                   runat="server">--%>
                    <%--<div class="form-group">--%>
                        <%--<div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="" runat="server" Text="Default"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="playlist-read-private"  runat="server" Text="playlist-read-private"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="playlist-read-collaborative" runat="server" Text="playlist-read-collaborative"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="playlist-modify-public" runat="server" Text="playlist-modify-public"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="playlist-modify-private" runat="server" Text="playlist-modify-private"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="streaming" runat="server" Text="streaming"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-follow-modify" runat="server" Text="user-follow-modify"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-follow-read" runat="server" Text="user-follow-read"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-library-read" runat="server" Text="user-library-read"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-library-modify" runat="server" Text="user-library-modify"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffS cope" type="checkbox" Value="user-read-private" runat="server" Text="user-read-privat"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-read-birthdate" runat="server" Text="user-read-birthdate"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-read-email" runat="server" Text="user-read-email"></asp:CheckBox></label></div>
                        <div class="checkbox"><label class="btn btn-default"><asp:CheckBox class="deffScope" type="checkbox" Value="user-top-read" runat="server" Text="user-top-read"></asp:CheckBox></label></div>--%>
                    <%--</div>--%>
              <%--</asp:CheckBoxList>--%>

                <%--<div class="row">
                    <div class="col-md-4 col-md-offset-4">--%>
                <table class="table">
                    <tr>
                    <td class="">
                    <div class="row m-row">
                        </div>
                        </td>
                    <td class="col-md-4">
                        <div class="row bordered">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 highlight">
                        <asp:CheckBoxList id="scopeList" class="scopeList" RepeatDirection="Vertical" CellPadding="10" CellSpacing="10" RepeatLayout="Flow" TextAlign="Right" runat="server">
                                <asp:ListItem class="playlistreadprivate" id="playlistreadprivate" type="checkbox" Value="playlist-read-private"  runat="server" Text="playlist-read-private"></asp:ListItem>
                                <asp:ListItem class="playlistreadcollaborative" id="playlistreadcollaborative" type="checkbox" Value="playlist-read-collaborative" runat="server" Text="playlist-read-collaborative"></asp:ListItem>
                                <asp:ListItem class="playlistmodifypublic" id="playlistmodifypublic" type="checkbox" Value="playlist-modify-public" runat="server" Text="playlist-modify-public"></asp:ListItem>
                                <asp:ListItem class="playlistmodifyprivate" id="playlistmodifyprivate" type="checkbox" Value="playlist-modify-private" runat="server" Text="playlist-modify-private"></asp:ListItem>
                                <asp:ListItem class="streaming" id="streaming" type="checkbox" Value="streaming" runat="server" Text="streaming"></asp:ListItem>
                                <asp:ListItem class="userfollowmodify" id="userfollowmodify" type="checkbox" Value="user-follow-modify" runat="server" Text="user-follow-modify"></asp:ListItem>
                                <asp:ListItem class="userfollowread" id="userfollowread" type="checkbox" Value="user-follow-read" runat="server" Text="user-follow-read"></asp:ListItem>
                                <asp:ListItem class="userlibraryread" id="userlibraryread" type="checkbox" Value="user-library-read" runat="server" Text="user-library-read"></asp:ListItem>
                                <asp:ListItem class="userlibrarymodify" id="userlibrarymodify" type="checkbox" Value="user-library-modify" runat="server" Text="user-library-modify"></asp:ListItem>
                                <asp:ListItem class="userreadprivate" id="userreadprivate" type="checkbox" Value="user-read-private" runat="server" Text="user-read-private"></asp:ListItem>
                                <asp:ListItem class="userreadbirthdate" id="userreadbirthdate" type="checkbox" Value="user-read-birthdate" runat="server" Text="user-read-birthdate"></asp:ListItem>
                                <asp:ListItem class="userreademail" id="userreademail" type="checkbox" Value="user-read-email" runat="server" Text="user-read-email"></asp:ListItem>
                                <asp:ListItem class="usertopread" id="usertopread" type="checkbox" Value="user-top-read" runat="server" Text="user-top-read"></asp:ListItem>
                        </asp:CheckBoxList>
                                </div>
                            </div>
                        </td>
                    <td class="">
                    <div class="row m-row">
                        </div>
                        </td>
                        </tr>
                </table>
                    <%--</div>
                </div>--%>

                <div class="form-group text-center">
                    <asp:DropDownList id="ApiList" class="ApiList" AutoPostBack="True" OnSelectedIndexChanged="Selection_Change" runat="server">
                        <%--<asp:ListItem Selected="True" Value="GetAlbumId"> Get Album Id </asp:ListItem>--%>
                        <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                        <asp:ListItem Value="GetAlbumId"> Get Album Id </asp:ListItem>
                        <asp:ListItem Value="GetAlbumIds"> Get Albums Ids </asp:ListItem>
                        <asp:ListItem Value="GetAlbumTracks"> Get Album Tracks </asp:ListItem>
                        <asp:ListItem Value="GetArtist"> Get An Artist </asp:ListItem>
                        <asp:ListItem Value="GetArtists"> Get Several Artists </asp:ListItem>
                        <asp:ListItem Value="GetArtistAlbums"> Get Artist's Albums </asp:ListItem>
                        <asp:ListItem Value="GetArtistTopTracks"> Get Artist's Top Tracks </asp:ListItem>
                        <asp:ListItem Value="GetArtistRelatedArtists"> Get Artist's Related Artists </asp:ListItem>
                        <asp:ListItem Value="GetAudioFeatureForTrack"> Get Audio Features For A Track </asp:ListItem>
                        <asp:ListItem Value="GetAudioFeatureForTracks"> Get Audio Features For Several Tracks </asp:ListItem>
                        <asp:ListItem Value="FeaturedPlaylists"> Featured Playlists </asp:ListItem>
                        <asp:ListItem Value="NewReleases"> New Releases </asp:ListItem>
                        <asp:ListItem Value="GetCategory"> Get Category </asp:ListItem>
                        <asp:ListItem Value="GetCategories"> Get Categories </asp:ListItem>
                        <asp:ListItem Value="GetCategoryPlaylists"> Get Category Playlists </asp:ListItem>
                        <asp:ListItem Value="GetMe"> Get Me </asp:ListItem>
                        <asp:ListItem Value="GetFollowedArtists"> Get Followed Artists </asp:ListItem>
                        <asp:ListItem Value="FollowArtistsOrUsers"> Follow Artists Or Users </asp:ListItem>
                        <asp:ListItem Value="UnfollowArtistsOrUsers"> Unfollow Artists Or Users </asp:ListItem>
                        <asp:ListItem Value="FollowingContainsArtistsOrUsers"> Check If Current User Follows Artists Or Users </asp:ListItem>
                        <asp:ListItem Value="FollowPlaylist"> Follow a Playlist </asp:ListItem>
                        <asp:ListItem Value="UnfollowPlaylist"> Unfollow a Playlist </asp:ListItem>
                        <asp:ListItem Value="SaveTracksForUser"> Save Tracks For User </asp:ListItem>
                        <asp:ListItem Value="GetUserSavedTracks"> Get User's Saved Tracks </asp:ListItem>
                        <asp:ListItem Value="RemoveUserSavedTracks"> Remove User's Saved Tracks </asp:ListItem>
                        <asp:ListItem Value="CheckUserSavedTracks"> Check User's Saved Tracks </asp:ListItem>
                        <asp:ListItem Value="SaveAlbumsForUser"> Save Albums for Current User </asp:ListItem>
                        <asp:ListItem Value="GetUserSavedAlbums"> Get Current User's Saved Albums </asp:ListItem>
                        <asp:ListItem Value="RemoveUserSavedAlbums"> Remove Albums For Current User </asp:ListItem>
                        <asp:ListItem Value="CheckUserSavedAlbums"> Check User's Saved Albums </asp:ListItem>
                        <asp:ListItem Value="GetUserTopArtistsAndTracks"> Get User's Top Artists and Tracks </asp:ListItem>
                        <asp:ListItem Value="GetRecommendationsBasedOnSeeds"> Get Recommendations Based on Seeds </asp:ListItem>
                    </asp:DropDownList>
                    <span id="ParamList" class="ParamList" runat="server"></span>
                </div>

                <%--<div class="form-group text-center">
                    <span id="ParamList" class="ParamList" runat="server"></span>
                </div>--%>

            <%--</div>--%>
                <%--<asp:Button id="SelectApiButton" runat="server" type="button" class="SelectApiButton btn btn-success" onclick="SelectAPI_Click" text="Select"></asp:Button>--%>
                <div class="text-center">
                    <asp:Button id="AuthorizeApiButton" runat="server" type="button" class="AuthorizeApiButton btn btn-success" onclick="AuthorizeButton_Click" text="Authorize"></asp:Button>
                </div>
            </div>
        </form>
    <%--</div>--%>
</asp:Content>
