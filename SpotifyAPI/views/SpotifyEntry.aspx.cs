﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;

namespace SpotifyAPI.views
{
    public partial class SpotifyEntry : System.Web.UI.Page
    {

        private static string apiUrl = "";

        public string country { get; set; }
        public string limit { get; set; }
        public string offset { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Need this foreach loop to retain ParamCollection variables after the page load
            foreach (string textboxId in ParamCollection)
            {
                var textbox = new TextBox { ID = textboxId };
                //mainForm.Controls.Add(textbox);
                ParamList.Controls.Add(textbox);
            }

            if (!IsPostBack)
            {
                // Validate initially to force asterisks
                // to appear before the first roundtrip.
                Validate();
            }

            //foreach (string dropdownlistId in DropDownListIdCollection)
            //{
            //    var dropdownlist = new DropDownList { ID = dropdownlistId };
            //    ParamList.Controls.Add(textbox);
            //}
        }

        protected void AuthorizeButton_Click(object sender, EventArgs e)
        {
            //Response.Redirect("https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=user-read-private&state=83jfdkd9dlslvx3w&redirect_uri=http://localhost:64270/views/Callbacks/Callback.aspx&show_dialog=true");
            //Response.Redirect("https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=user-follow-read,playlist-read-private&state=83jfdkd9dlslvx3w&redirect_uri=http://localhost:64270/views/Callbacks/Callback.aspx&show_dialog=true");

            List<ListItem> selected = new List<ListItem>();
            string itemValue = "";
            foreach (ListItem item in scopeList.Items)
                if (item.Selected)
                {
                    selected.Add(item);
                    //System.Diagnostics.Debug.WriteLine("Scopes: " + item);
                    itemValue += item + " ";
                }
            System.Diagnostics.Debug.WriteLine("Scope List: " + itemValue);

            if (ApiList.SelectedItem.Value == "")
            {
                AuthorizeApiButton.Enabled = false;
            }
            else
            {
                //Response.Redirect("https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=user-follow-modify&state=83jfdkd9dlslvx3w&redirect_uri=http://localhost:64270/views/Callbacks/Callback.aspx&show_dialog=true");
                var encodedScopes = Uri.EscapeDataString(itemValue);
                //var urlToGo = "https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=" + encodedScopes + "&state=83jfdkd9dlslvx3w&redirect_uri=http://localhost:64270/views/Callbacks/Callback.aspx&show_dialog=true";
                var urlToGo = "https://accounts.spotify.com/authorize?response_type=code&client_id=f44b4cfe95144403bb44c3e00de01e29&scope=" + encodedScopes + "&state=83jfdkd9dlslvx3w&redirect_uri=" + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/views/Callbacks/Callback.aspx&show_dialog=true";

                //foreach (Control ctr in mainForm.Controls)
                foreach (Control ctr in ParamList.Controls)
                {
                    if (ctr is TextBox)
                    {
                        string tbID = ((TextBox)ctr).ID;
                        string tbvalue = ((TextBox)ctr).Text;
                        //System.Diagnostics.Debug.WriteLine(tbID);
                        //System.Diagnostics.Debug.WriteLine(tbvalue);
                        Session[tbID] = tbvalue;
                    }

                    //if (ctr is DropDownList)
                    //{
                    //    string ddlID = ((DropDownList)ctr).ID;
                    //    string ddlvalue = ((DropDownList)ctr).Text;
                    //    //System.Diagnostics.Debug.WriteLine(ddlID);
                    //    //System.Diagnostics.Debug.WriteLine(ddlvalue);
                    //    Session[ddlID] = ddlvalue;
                    //}
                }

                foreach (var crntSession in Session)
                {
                    System.Diagnostics.Debug.WriteLine(string.Concat(crntSession, "=", Session[crntSession.ToString()]) + "<br />");
                }

                System.Diagnostics.Debug.WriteLine(urlToGo);
                Response.Redirect(urlToGo);
            }
        }

        protected void Selection_Change(object sender, EventArgs e)
        {
            var collection = new List<string>();
            //int total;

            //SelectApiButton.Enabled = true;
            AuthorizeApiButton.Enabled = true;
            if (ApiList.SelectedItem.Value == "GetAlbumId")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Albums/GetAlbum.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Id: <br />"));
                TextBox AlbumId = new TextBox { ID = "albumId", Text = "" };
                AlbumId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(AlbumId.ID);
                ParamList.Controls.Add(AlbumId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetAlbumIds");
            }
            else if (ApiList.SelectedItem.Value == "GetAlbumIds")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Albums/GetAlbums.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Ids: <br />"));
                TextBox AlbumIds = new TextBox { ID = "albumIds", Text = "" };
                AlbumIds.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(AlbumIds.ID);
                ParamList.Controls.Add(AlbumIds);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetAlbumIds");
            }
            else if (ApiList.SelectedItem.Value == "GetAlbumTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Albums/GetAlbumTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Id: <br />"));
                TextBox AlbumId = new TextBox { ID = "albumId", Text = "" };
                AlbumId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(AlbumId.ID);
                ParamList.Controls.Add(AlbumId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetAlbumTracks");
            }
            else if (ApiList.SelectedItem.Value == "GetArtist")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Artists/GetArtist.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Artist Id: <br />"));
                TextBox ArtistId = new TextBox { ID = "artistId", Text = "" };
                ArtistId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(ArtistId.ID);
                ParamList.Controls.Add(ArtistId);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetArtist");
            }
            else if (ApiList.SelectedItem.Value == "GetArtists")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Artists/GetArtists.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Artist Ids: <br />"));
                TextBox ArtistIds = new TextBox { ID = "artistIds", Text = "" };
                ArtistIds.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(ArtistIds.ID);
                ParamList.Controls.Add(ArtistIds);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetArtists");
            }
            else if (ApiList.SelectedItem.Value == "GetArtistAlbums")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Artists/GetArtistAlbums.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Artist Id: <br />"));
                TextBox ArtistId = new TextBox { ID = "artistId", Text = "" };
                ArtistId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(ArtistId.ID);
                ParamList.Controls.Add(ArtistId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Type: <br />"));
                DropDownList AlbumType = new DropDownList { ID = "albumType" };
                AlbumType.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                AlbumType.DataSource = getAlbumTypeData();
                AlbumType.DataTextField = "Value";
                AlbumType.DataValueField = "Key";
                AlbumType.DataBind();

                collection.Add(AlbumType.ID);
                ParamList.Controls.Add(AlbumType);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetArtistAlbums");
            }
            else if (ApiList.SelectedItem.Value == "GetArtistTopTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Artists/GetArtistTopTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Artist Id: <br />"));
                TextBox ArtistId = new TextBox { ID = "artistId", Text = "" };
                ArtistId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(ArtistId.ID);
                ParamList.Controls.Add(ArtistId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetArtistTopTracks");
            }
            else if (ApiList.SelectedItem.Value == "GetArtistRelatedArtists")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Artists/GetArtistRelatedArtists.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Artist Id: <br />"));
                TextBox ArtistId = new TextBox { ID = "artistId", Text = "" };
                ArtistId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(ArtistId.ID);
                ParamList.Controls.Add(ArtistId);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetArtistRelatedArtists");
            }
            else if (ApiList.SelectedItem.Value == "GetAudioFeatureForTrack")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Audio-Features/GetAudioFeatureForTrack.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Track Id: <br />"));
                TextBox TrackId = new TextBox { ID = "trackId", Text = "" };
                TrackId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(TrackId.ID);
                ParamList.Controls.Add(TrackId);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetAudioFeatureForTrack");
            }
            else if (ApiList.SelectedItem.Value == "GetAudioFeatureForTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Audio-Features/GetAudioFeatureForTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Track Ids: <br />"));
                TextBox TrackIds = new TextBox { ID = "trackIds", Text = "" };
                TrackIds.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(TrackIds.ID);
                ParamList.Controls.Add(TrackIds);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetAudioFeatureForTracks");
            }
            else if (ApiList.SelectedItem.Value == "FeaturedPlaylists")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Browse/FeaturedPlaylists.aspx";

                //ParamList.Controls.Add(new LiteralControl("<br /><br /> TimeStampTable: <br />"));
                //Table TimeStampTable = new Table { ID = "timeStampTable" };
                //TimeStampTable.Attributes["class"] = "timeStampTable col-md-1 col-md-offset-5 col-xs-5 col-xs-offset-1";

                //collection.Add(TimeStampTable.ID);
                //ParamList.Controls.Add(TimeStampTable);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Locale: <br />"));
                DropDownList Locale_1 = new DropDownList { ID = "locale_1" };
                Locale_1.Attributes["class"] = "locale_1 col-md-1 col-md-offset-5 col-xs-5 col-xs-offset-1";

                Locale_1.DataSource = getLanguageCode();
                Locale_1.DataTextField = "Value";
                Locale_1.DataValueField = "Key";
                Locale_1.DataBind();

                collection.Add(Locale_1.ID);
                ParamList.Controls.Add(Locale_1);

                DropDownList Locale_2 = new DropDownList { ID = "locale_2" };
                Locale_2.Attributes["class"] = "locale_2 col-md-1 col-xs-5";

                Locale_2.DataSource = getCountryCode();
                Locale_2.DataTextField = "Value";
                Locale_2.DataValueField = "Key";
                Locale_2.DataBind();

                collection.Add(Locale_2.ID);
                ParamList.Controls.Add(Locale_2);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                //ParamList.Controls.Add(new LiteralControl("<br /><br /> TimeStamp: <br />"));
                ParamList.Controls.Add(new LiteralControl("<br /><br /> Date: <br />"));
                TextBox Date = new TextBox { ID = "date", Text = "" };
                Date.Text = DateTime.Today.ToString("yyyy-MM-dd");
                Date.Attributes["class"] = "datepicker col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Date.ID);
                ParamList.Controls.Add(Date);

                //DropDownList Hours = new DropDownList { ID = "hours" };
                //Hours.Attributes["class"] = "locale_2 col-md-1 col-xs-5";

                //Hours.DataSource = getHours();
                //Hours.DataTextField = "Value";
                //Hours.DataValueField = "Key";
                //Hours.DataBind();

                //collection.Add(Hours.ID);
                //ParamList.Controls.Add(Hours);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Hours: <br />"));
                DropDownList Hours = new DropDownList { ID = "hours" };
                Hours.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Hours.DataSource = getHours();
                Hours.DataTextField = "Value";
                Hours.DataValueField = "Key";
                Hours.DataBind();

                collection.Add(Hours.ID);
                ParamList.Controls.Add(Hours);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Minutes: <br />"));
                DropDownList Minutes = new DropDownList { ID = "minutes" };
                Minutes.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Minutes.DataSource = getMinAndSec();
                Minutes.DataTextField = "Value";
                Minutes.DataValueField = "Key";
                Minutes.DataBind();

                collection.Add(Minutes.ID);
                ParamList.Controls.Add(Minutes);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Seconds: <br />"));
                DropDownList Seconds = new DropDownList { ID = "seconds" };
                Seconds.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Seconds.DataSource = getMinAndSec();
                Seconds.DataTextField = "Value";
                Seconds.DataValueField = "Key";
                Seconds.DataBind();

                collection.Add(Seconds.ID);
                ParamList.Controls.Add(Seconds);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is FeaturedPlaylists");
            }
            else if (ApiList.SelectedItem.Value == "NewReleases")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Browse/NewReleases.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limits: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is NewReleases");
            }
            else if (ApiList.SelectedItem.Value == "GetCategory")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Browse/GetCategory.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Category ID: <br />"));
                TextBox CategoryId = new TextBox { ID = "categoryId", Text = "" };
                CategoryId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(CategoryId.ID);
                ParamList.Controls.Add(CategoryId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Locale: <br />"));
                DropDownList Locale_1 = new DropDownList { ID = "locale_1" };
                Locale_1.Attributes["class"] = "locale_1 col-md-1 col-md-offset-5 col-xs-5 col-xs-offset-1";

                Locale_1.DataSource = getLanguageCode();
                Locale_1.DataTextField = "Value";
                Locale_1.DataValueField = "Key";
                Locale_1.DataBind();

                collection.Add(Locale_1.ID);
                ParamList.Controls.Add(Locale_1);

                DropDownList Locale_2 = new DropDownList { ID = "locale_2" };
                Locale_2.Attributes["class"] = "locale_2 col-md-1 col-xs-5";

                Locale_2.DataSource = getCountryCode();
                Locale_2.DataTextField = "Value";
                Locale_2.DataValueField = "Key";
                Locale_2.DataBind();

                collection.Add(Locale_2.ID);
                ParamList.Controls.Add(Locale_2);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetCategory");
            }
            else if (ApiList.SelectedItem.Value == "GetCategories")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Browse/GetCategories.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Locale: <br />"));
                DropDownList Locale_1 = new DropDownList { ID = "locale_1" };
                Locale_1.Attributes["class"] = "locale_1 col-md-1 col-md-offset-5 col-xs-5 col-xs-offset-1";

                Locale_1.DataSource = getLanguageCode();
                Locale_1.DataTextField = "Value";
                Locale_1.DataValueField = "Key";
                Locale_1.DataBind();

                collection.Add(Locale_1.ID);
                ParamList.Controls.Add(Locale_1);

                DropDownList Locale_2 = new DropDownList { ID = "locale_2" };
                Locale_2.Attributes["class"] = "locale_2 col-md-1 col-xs-5";

                Locale_2.DataSource = getCountryCode();
                Locale_2.DataTextField = "Value";
                Locale_2.DataValueField = "Key";
                Locale_2.DataBind();

                collection.Add(Locale_2.ID);
                ParamList.Controls.Add(Locale_2);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetCategories");
            }
            else if (ApiList.SelectedItem.Value == "GetCategoryPlaylists")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Browse/GetCategoryPlaylists.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Category ID: <br />"));
                TextBox CategoryId = new TextBox { ID = "categoryId", Text = "" };
                CategoryId.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(CategoryId.ID);
                ParamList.Controls.Add(CategoryId);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Country: <br />"));
                DropDownList Country = new DropDownList { ID = "country" };
                Country.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Country.DataSource = getCountryCode();
                Country.DataTextField = "Value";
                Country.DataValueField = "Key";
                Country.DataBind();

                collection.Add(Country.ID);
                ParamList.Controls.Add(Country);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetCategoryPlaylists");
            }
            else if (ApiList.SelectedItem.Value == "GetMe")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/GetMe.aspx";
                userreadprivate.Selected = true;
                userreademail.Selected = true;
                userreadbirthdate.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is GetMe");
            }
            else if (ApiList.SelectedItem.Value == "GetFollowedArtists")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/GetFollowedArtists.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Type: <br />"));
                TextBox Type = new TextBox { ID = "type", Text = "artist" };
                Type.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Type.ID);
                ParamList.Controls.Add(Type);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> After: <br />"));
                TextBox After = new TextBox { ID = "after", Text = "" };
                After.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(After.ID);
                ParamList.Controls.Add(After);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limit: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userfollowread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is GetFollowedArtists");
            }
            else if (ApiList.SelectedItem.Value == "FollowArtistsOrUsers")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/FollowArtistsOrUsers.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Type: <br />"));
                DropDownList Type = new DropDownList { ID = "type" };
                Type.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Type.DataSource = getItemType();
                Type.DataTextField = "Value";
                Type.DataValueField = "Key";
                Type.DataBind();

                collection.Add(Type.ID);
                ParamList.Controls.Add(Type);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Ids: <br />"));
                TextBox Ids = new TextBox { ID = "ids", Text = "" };
                Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Ids.ID);
                ParamList.Controls.Add(Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userfollowmodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is FollowArtistsOrUsers");
            }
            else if (ApiList.SelectedItem.Value == "UnfollowArtistsOrUsers")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/UnfollowArtistsOrUsers.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Type: <br />"));
                DropDownList Type = new DropDownList { ID = "type" };
                Type.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Type.DataSource = getItemType();
                Type.DataTextField = "Value";
                Type.DataValueField = "Key";
                Type.DataBind();

                collection.Add(Type.ID);
                ParamList.Controls.Add(Type);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Ids: <br />"));
                TextBox Ids = new TextBox { ID = "ids", Text = "" };
                Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Ids.ID);
                ParamList.Controls.Add(Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userfollowmodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is UnfollowArtistsOrUsers");
            }
            else if (ApiList.SelectedItem.Value == "FollowingContainsArtistsOrUsers")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/FollowingContainsArtistsOrUsers.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Type: <br />"));
                DropDownList Type = new DropDownList { ID = "type" };
                Type.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Type.DataSource = getItemType();
                Type.DataTextField = "Value";
                Type.DataValueField = "Key";
                Type.DataBind();

                collection.Add(Type.ID);
                ParamList.Controls.Add(Type);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Ids: <br />"));
                TextBox Ids = new TextBox { ID = "ids", Text = "" };
                Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Ids.ID);
                ParamList.Controls.Add(Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userfollowread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is FollowingContainsArtistsOrUsers");
            }
            else if (ApiList.SelectedItem.Value == "FollowPlaylist")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Users/FollowPlaylist.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Public: <br />"));
                DropDownList Public = new DropDownList { ID = "public" };
                Public.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Public.DataSource = trueOrFalse();
                Public.DataTextField = "Value";
                Public.DataValueField = "Key";
                Public.DataBind();

                collection.Add(Public.ID);
                ParamList.Controls.Add(Public);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Owner Id: <br />"));
                TextBox Owner_Id = new TextBox { ID = "owner_id", Text = "" };
                Owner_Id.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Owner_Id.ID);
                ParamList.Controls.Add(Owner_Id);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Playlist Id: <br />"));
                TextBox Playlist_Id = new TextBox { ID = "playlist_id", Text = "" };
                Playlist_Id.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Playlist_Id.ID);
                ParamList.Controls.Add(Playlist_Id);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                playlistmodifypublic.Selected = true;
                playlistmodifyprivate.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is FollowPlaylist");
            }
            else if (ApiList.SelectedItem.Value == "UnfollowPlaylist")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Users/UnfollowPlaylist.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Owner Id: <br />"));
                TextBox Owner_Id = new TextBox { ID = "owner_id", Text = "" };
                Owner_Id.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Owner_Id.ID);
                ParamList.Controls.Add(Owner_Id);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Playlist Id: <br />"));
                TextBox Playlist_Id = new TextBox { ID = "playlist_id", Text = "" };
                Playlist_Id.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Playlist_Id.ID);
                ParamList.Controls.Add(Playlist_Id);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                playlistmodifypublic.Selected = true;
                playlistmodifyprivate.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is UnfollowPlaylist");
            }
            else if (ApiList.SelectedItem.Value == "SaveTracksForUser")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/SaveTracksForUser.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Track Ids: <br />"));
                TextBox Track_Ids = new TextBox { ID = "track_ids", Text = "" };
                Track_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Track_Ids.ID);
                ParamList.Controls.Add(Track_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibrarymodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is SaveTracksForUser");
            }
            else if (ApiList.SelectedItem.Value == "GetUserSavedTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/GetUserSavedTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limits: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibraryread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is GetUserSavedTracks");
            }
            else if (ApiList.SelectedItem.Value == "RemoveUserSavedTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/RemoveUserSavedTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Track Ids: <br />"));
                TextBox Track_Ids = new TextBox { ID = "track_ids", Text = "" };
                Track_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Track_Ids.ID);
                ParamList.Controls.Add(Track_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibrarymodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is RemoveUserSavedTracks");
            }
            else if (ApiList.SelectedItem.Value == "CheckUserSavedTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/CheckUserSavedTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Track Ids: <br />"));
                TextBox Track_Ids = new TextBox { ID = "track_ids", Text = "" };
                Track_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Track_Ids.ID);
                ParamList.Controls.Add(Track_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibrarymodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is CheckUserSavedTracks");
            }
            else if (ApiList.SelectedItem.Value == "SaveAlbumsForUser")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/SaveAlbumsForUser.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Ids: <br />"));
                TextBox Album_Ids = new TextBox { ID = "album_ids", Text = "" };
                Album_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Album_Ids.ID);
                ParamList.Controls.Add(Album_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibrarymodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is SaveAlbumsForUser");
            }
            else if (ApiList.SelectedItem.Value == "GetUserSavedAlbums")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/GetUserSavedAlbums.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limits: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibraryread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is GetUserSavedAlbums");
            }
            else if (ApiList.SelectedItem.Value == "CheckUserSavedAlbums")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/CheckUserSavedAlbums.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Ids: <br />"));
                TextBox Album_Ids = new TextBox { ID = "album_ids", Text = "" };
                Album_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Album_Ids.ID);
                ParamList.Controls.Add(Album_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibraryread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is CheckUserSavedAlbums");
            }
            else if (ApiList.SelectedItem.Value == "RemoveUserSavedAlbums")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/RemoveUserSavedAlbums.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Album Ids: <br />"));
                TextBox Album_Ids = new TextBox { ID = "album_ids", Text = "" };
                Album_Ids.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Album_Ids.ID);
                ParamList.Controls.Add(Album_Ids);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                userlibrarymodify.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is RemoveUserSavedAlbums");
            }
            else if (ApiList.SelectedItem.Value == "GetUserTopArtistsAndTracks")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Me/GetUserTopArtistsAndTracks.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Time Range: <br />"));
                DropDownList TimeRange = new DropDownList { ID = "time_range" };
                TimeRange.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TimeRange.DataSource = getTimeRange();
                TimeRange.DataTextField = "Value";
                TimeRange.DataValueField = "Key";
                TimeRange.DataBind();

                collection.Add(TimeRange.ID);
                ParamList.Controls.Add(TimeRange);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Type: <br />"));
                DropDownList Type = new DropDownList { ID = "type" };
                Type.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Type.DataSource = getEntityTypes();
                Type.DataTextField = "Value";
                Type.DataValueField = "Key";
                Type.DataBind();

                collection.Add(Type.ID);
                ParamList.Controls.Add(Type);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limits: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Offset: <br />"));
                TextBox Offset = new TextBox { ID = "offset", Text = "" };
                Offset.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Offset.ID);
                ParamList.Controls.Add(Offset);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                usertopread.Selected = true;
                System.Diagnostics.Debug.WriteLine("Api Call is GetUserTopArtistsAndTracks");
            }
            else if (ApiList.SelectedItem.Value == "GetRecommendationsBasedOnSeeds")
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "../Recommendations/GetRecommendationsBasedOnSeeds.aspx";

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Limits: <br />"));
                TextBox Limit = new TextBox { ID = "limit", Text = "" };
                Limit.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(Limit.ID);
                ParamList.Controls.Add(Limit);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Market: <br />"));
                DropDownList Market = new DropDownList { ID = "market" };
                Market.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                Market.DataSource = getCountryCode();
                Market.DataTextField = "Value";
                Market.DataValueField = "Key";
                Market.DataBind();

                collection.Add(Market.ID);
                ParamList.Controls.Add(Market);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Acousticness: <br />"));
                DropDownList MinAcousticness = new DropDownList { ID = "min_acousticness" };
                MinAcousticness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinAcousticness.DataSource = getTrackAttributesGrade();
                MinAcousticness.DataTextField = "Value";
                MinAcousticness.DataValueField = "Key";
                MinAcousticness.DataBind();

                collection.Add(MinAcousticness.ID);
                ParamList.Controls.Add(MinAcousticness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Acousticness: <br />"));
                DropDownList MaxAcousticness = new DropDownList { ID = "max_acousticness" };
                MaxAcousticness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxAcousticness.DataSource = getTrackAttributesGrade();
                MaxAcousticness.DataTextField = "Value";
                MaxAcousticness.DataValueField = "Key";
                MaxAcousticness.DataBind();

                collection.Add(MaxAcousticness.ID);
                ParamList.Controls.Add(MaxAcousticness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Acousticness: <br />"));
                DropDownList TargetAcousticness = new DropDownList { ID = "target_acousticness" };
                TargetAcousticness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetAcousticness.DataSource = getTrackAttributesGrade();
                TargetAcousticness.DataTextField = "Value";
                TargetAcousticness.DataValueField = "Key";
                TargetAcousticness.DataBind();

                collection.Add(TargetAcousticness.ID);
                ParamList.Controls.Add(TargetAcousticness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Danceability: <br />"));
                DropDownList MinDanceability = new DropDownList { ID = "min_danceability" };
                MinDanceability.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinDanceability.DataSource = getTrackAttributesGrade();
                MinDanceability.DataTextField = "Value";
                MinDanceability.DataValueField = "Key";
                MinDanceability.DataBind();

                collection.Add(MinDanceability.ID);
                ParamList.Controls.Add(MinDanceability);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Danceability: <br />"));
                DropDownList MaxDanceability = new DropDownList { ID = "max_danceability" };
                MaxDanceability.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxDanceability.DataSource = getTrackAttributesGrade();
                MaxDanceability.DataTextField = "Value";
                MaxDanceability.DataValueField = "Key";
                MaxDanceability.DataBind();

                collection.Add(MaxDanceability.ID);
                ParamList.Controls.Add(MaxDanceability);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Danceability: <br />"));
                DropDownList TargetDanceability = new DropDownList { ID = "target_danceability" };
                TargetDanceability.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetDanceability.DataSource = getTrackAttributesGrade();
                TargetDanceability.DataTextField = "Value";
                TargetDanceability.DataValueField = "Key";
                TargetDanceability.DataBind();

                collection.Add(TargetDanceability.ID);
                ParamList.Controls.Add(TargetDanceability);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Duration (ms): <br />"));
                TextBox MinDuration = new TextBox { ID = "min_duration_ms", Text = "" };
                MinDuration.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(MinDuration.ID);
                ParamList.Controls.Add(MinDuration);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Duration (ms): <br />"));
                TextBox MaxDuration = new TextBox { ID = "max_duration_ms", Text = "" };
                MaxDuration.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(MaxDuration.ID);
                ParamList.Controls.Add(MaxDuration);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Duration (ms): <br />"));
                TextBox TargetDuration = new TextBox { ID = "target_duration_ms", Text = "" };
                TargetDuration.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";
                collection.Add(TargetDuration.ID);
                ParamList.Controls.Add(TargetDuration);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Energy: <br />"));
                DropDownList MinEnergy = new DropDownList { ID = "min_energy" };
                MinEnergy.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinEnergy.DataSource = getTrackAttributesGrade();
                MinEnergy.DataTextField = "Value";
                MinEnergy.DataValueField = "Key";
                MinEnergy.DataBind();

                collection.Add(MinEnergy.ID);
                ParamList.Controls.Add(MinEnergy);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Energy: <br />"));
                DropDownList MaxEnergy = new DropDownList { ID = "max_energy" };
                MaxEnergy.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxEnergy.DataSource = getTrackAttributesGrade();
                MaxEnergy.DataTextField = "Value";
                MaxEnergy.DataValueField = "Key";
                MaxEnergy.DataBind();

                collection.Add(MaxEnergy.ID);
                ParamList.Controls.Add(MaxEnergy);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Energy: <br />"));
                DropDownList TargetEnergy = new DropDownList { ID = "target_energy" };
                TargetEnergy.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetEnergy.DataSource = getTrackAttributesGrade();
                TargetEnergy.DataTextField = "Value";
                TargetEnergy.DataValueField = "Key";
                TargetEnergy.DataBind();

                collection.Add(TargetEnergy.ID);
                ParamList.Controls.Add(TargetEnergy);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Instrumentalness: <br />"));
                DropDownList MinInstrumentalness = new DropDownList { ID = "min_instrumentalness" };
                MinInstrumentalness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinInstrumentalness.DataSource = getTrackAttributesGrade();
                MinInstrumentalness.DataTextField = "Value";
                MinInstrumentalness.DataValueField = "Key";
                MinInstrumentalness.DataBind();

                collection.Add(MinInstrumentalness.ID);
                ParamList.Controls.Add(MinInstrumentalness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Instrumentalness: <br />"));
                DropDownList MaxInstrumentalness = new DropDownList { ID = "max_instrumentalness" };
                MaxInstrumentalness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxInstrumentalness.DataSource = getTrackAttributesGrade();
                MaxInstrumentalness.DataTextField = "Value";
                MaxInstrumentalness.DataValueField = "Key";
                MaxInstrumentalness.DataBind();

                collection.Add(MaxInstrumentalness.ID);
                ParamList.Controls.Add(MaxInstrumentalness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Instrumentalness: <br />"));
                DropDownList TargetInstrumentalness = new DropDownList { ID = "target_instrumentalness" };
                TargetInstrumentalness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetInstrumentalness.DataSource = getTrackAttributesGrade();
                TargetInstrumentalness.DataTextField = "Value";
                TargetInstrumentalness.DataValueField = "Key";
                TargetInstrumentalness.DataBind();

                collection.Add(TargetInstrumentalness.ID);
                ParamList.Controls.Add(TargetInstrumentalness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Key: <br />"));
                DropDownList MinKey = new DropDownList { ID = "min_key" };
                MinKey.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinKey.DataSource = getKeyTracks();
                MinKey.DataTextField = "Value";
                MinKey.DataValueField = "Key";
                MinKey.DataBind();

                collection.Add(MinKey.ID);
                ParamList.Controls.Add(MinKey);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Key: <br />"));
                DropDownList MaxKey = new DropDownList { ID = "max_key" };
                MaxKey.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxKey.DataSource = getKeyTracks();
                MaxKey.DataTextField = "Value";
                MaxKey.DataValueField = "Key";
                MaxKey.DataBind();

                collection.Add(MaxKey.ID);
                ParamList.Controls.Add(MaxKey);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Key: <br />"));
                DropDownList TargetKey = new DropDownList { ID = "target_key" };
                TargetKey.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetKey.DataSource = getKeyTracks();
                TargetKey.DataTextField = "Value";
                TargetKey.DataValueField = "Key";
                TargetKey.DataBind();

                collection.Add(TargetKey.ID);
                ParamList.Controls.Add(TargetKey);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Liveness: <br />"));
                DropDownList MinLiveness = new DropDownList { ID = "min_liveness" };
                MinLiveness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinLiveness.DataSource = getTrackAttributesGrade();
                MinLiveness.DataTextField = "Value";
                MinLiveness.DataValueField = "Key";
                MinLiveness.DataBind();

                collection.Add(MinLiveness.ID);
                ParamList.Controls.Add(MinLiveness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Liveness: <br />"));
                DropDownList MaxLiveness = new DropDownList { ID = "max_liveness" };
                MaxLiveness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxLiveness.DataSource = getTrackAttributesGrade();
                MaxLiveness.DataTextField = "Value";
                MaxLiveness.DataValueField = "Key";
                MaxLiveness.DataBind();

                collection.Add(MaxLiveness.ID);
                ParamList.Controls.Add(MaxLiveness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Liveness: <br />"));
                DropDownList TargetLiveness = new DropDownList { ID = "target_liveness" };
                TargetLiveness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetLiveness.DataSource = getTrackAttributesGrade();
                TargetLiveness.DataTextField = "Value";
                TargetLiveness.DataValueField = "Key";
                TargetLiveness.DataBind();

                collection.Add(TargetLiveness.ID);
                ParamList.Controls.Add(TargetLiveness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Loudness: <br />"));
                DropDownList MinLoudness = new DropDownList { ID = "min_loudness" };
                MinLoudness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinLoudness.DataSource = getDecibels();
                MinLoudness.DataTextField = "Value";
                MinLoudness.DataValueField = "Key";
                MinLoudness.DataBind();

                collection.Add(MinLoudness.ID);
                ParamList.Controls.Add(MinLoudness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Loudness: <br />"));
                DropDownList MaxLoudness = new DropDownList { ID = "max_loudness" };
                MaxLoudness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxLoudness.DataSource = getDecibels();
                MaxLoudness.DataTextField = "Value";
                MaxLoudness.DataValueField = "Key";
                MaxLoudness.DataBind();

                collection.Add(MaxLoudness.ID);
                ParamList.Controls.Add(MaxLoudness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Loudness: <br />"));
                DropDownList TargetLoudness = new DropDownList { ID = "target_loudness" };
                TargetLoudness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetLoudness.DataSource = getDecibels();
                TargetLoudness.DataTextField = "Value";
                TargetLoudness.DataValueField = "Key";
                TargetLoudness.DataBind();

                collection.Add(TargetLoudness.ID);
                ParamList.Controls.Add(TargetLoudness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Mode: <br />"));
                DropDownList MinMode = new DropDownList { ID = "min_mode" };
                MinMode.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinMode.DataSource = getModality();
                MinMode.DataTextField = "Value";
                MinMode.DataValueField = "Key";
                MinMode.DataBind();

                collection.Add(MinMode.ID);
                ParamList.Controls.Add(MinMode);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Mode: <br />"));
                DropDownList MaxMode = new DropDownList { ID = "max_mode" };
                MaxMode.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxMode.DataSource = getModality();
                MaxMode.DataTextField = "Value";
                MaxMode.DataValueField = "Key";
                MaxMode.DataBind();

                collection.Add(MaxMode.ID);
                ParamList.Controls.Add(MaxMode);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Mode: <br />"));
                DropDownList TargetMode = new DropDownList { ID = "target_mode" };
                TargetMode.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetMode.DataSource = getModality();
                TargetMode.DataTextField = "Value";
                TargetMode.DataValueField = "Key";
                TargetMode.DataBind();

                collection.Add(TargetMode.ID);
                ParamList.Controls.Add(TargetMode);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Popularity: <br />"));
                DropDownList MinPopularity = new DropDownList { ID = "min_popularity" };
                MinPopularity.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinPopularity.DataSource = getZeroToHundred();
                MinPopularity.DataTextField = "Value";
                MinPopularity.DataValueField = "Key";
                MinPopularity.DataBind();

                collection.Add(MinPopularity.ID);
                ParamList.Controls.Add(MinPopularity);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Popularity: <br />"));
                DropDownList MaxPopularity = new DropDownList { ID = "max_popularity" };
                MaxPopularity.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxPopularity.DataSource = getZeroToHundred();
                MaxPopularity.DataTextField = "Value";
                MaxPopularity.DataValueField = "Key";
                MaxPopularity.DataBind();

                collection.Add(MaxPopularity.ID);
                ParamList.Controls.Add(MaxPopularity);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Popularity: <br />"));
                DropDownList TargetPopularity = new DropDownList { ID = "target_popularity" };
                TargetPopularity.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetPopularity.DataSource = getZeroToHundred();
                TargetPopularity.DataTextField = "Value";
                TargetPopularity.DataValueField = "Key";
                TargetPopularity.DataBind();

                collection.Add(TargetPopularity.ID);
                ParamList.Controls.Add(TargetPopularity);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Speechiness: <br />"));
                DropDownList MinSpeechiness = new DropDownList { ID = "min_speechiness" };
                MinSpeechiness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinSpeechiness.DataSource = getFloatZeroToOne();
                MinSpeechiness.DataTextField = "Value";
                MinSpeechiness.DataValueField = "Key";
                MinSpeechiness.DataBind();

                collection.Add(MinSpeechiness.ID);
                ParamList.Controls.Add(MinSpeechiness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Speechiness: <br />"));
                DropDownList MaxSpeechiness = new DropDownList { ID = "max_speechiness" };
                MaxSpeechiness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxSpeechiness.DataSource = getFloatZeroToOne();
                MaxSpeechiness.DataTextField = "Value";
                MaxSpeechiness.DataValueField = "Key";
                MaxSpeechiness.DataBind();

                collection.Add(MaxSpeechiness.ID);
                ParamList.Controls.Add(MaxSpeechiness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Speechiness: <br />"));
                DropDownList TargetSpeechiness = new DropDownList { ID = "target_speechiness" };
                TargetSpeechiness.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetSpeechiness.DataSource = getFloatZeroToOne();
                TargetSpeechiness.DataTextField = "Value";
                TargetSpeechiness.DataValueField = "Key";
                TargetSpeechiness.DataBind();

                collection.Add(TargetSpeechiness.ID);
                ParamList.Controls.Add(TargetSpeechiness);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Tempo: <br />"));
                DropDownList MinTempo = new DropDownList { ID = "min_tempo", Text = "" };
                MinTempo.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinTempo.DataSource = getFloatZeroToFiveHundred();
                MinTempo.DataTextField = "Value";
                MinTempo.DataValueField = "Key";
                MinTempo.DataBind();

                collection.Add(MinTempo.ID);
                ParamList.Controls.Add(MinTempo);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Tempo: <br />"));
                DropDownList MaxTempo = new DropDownList { ID = "max_tempo", Text = "" };
                MaxTempo.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxTempo.DataSource = getFloatZeroToFiveHundred();
                MaxTempo.DataTextField = "Value";
                MaxTempo.DataValueField = "Key";
                MaxTempo.DataBind();

                collection.Add(MaxTempo.ID);
                ParamList.Controls.Add(MaxTempo);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Tempo: <br />"));
                DropDownList TargetTempo = new DropDownList { ID = "target_tempo", Text = "" };
                TargetTempo.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetTempo.DataSource = getFloatZeroToFiveHundred();
                TargetTempo.DataTextField = "Value";
                TargetTempo.DataValueField = "Key";
                TargetTempo.DataBind();

                collection.Add(TargetTempo.ID);
                ParamList.Controls.Add(TargetTempo);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Time Signature: <br />"));
                DropDownList MinTimeSignature = new DropDownList { ID = "min_time_signature", Text = "" };
                MinTimeSignature.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinTimeSignature.DataSource = getFloatZeroToFiveHundred();
                MinTimeSignature.DataTextField = "Value";
                MinTimeSignature.DataValueField = "Key";
                MinTimeSignature.DataBind();

                collection.Add(MinTimeSignature.ID);
                ParamList.Controls.Add(MinTimeSignature);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Time Signature: <br />"));
                DropDownList MaxTimeSignature = new DropDownList { ID = "max_time_signature", Text = "" };
                MaxTimeSignature.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxTimeSignature.DataSource = getFloatZeroToFiveHundred();
                MaxTimeSignature.DataTextField = "Value";
                MaxTimeSignature.DataValueField = "Key";
                MaxTimeSignature.DataBind();

                collection.Add(MaxTimeSignature.ID);
                ParamList.Controls.Add(MaxTimeSignature);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Time Signature: <br />"));
                DropDownList TargetTimeSignature = new DropDownList { ID = "target_time_signature", Text = "" };
                TargetTimeSignature.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetTimeSignature.DataSource = getFloatZeroToFiveHundred();
                TargetTimeSignature.DataTextField = "Value";
                TargetTimeSignature.DataValueField = "Key";
                TargetTimeSignature.DataBind();

                collection.Add(TargetTimeSignature.ID);
                ParamList.Controls.Add(TargetTimeSignature);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Min. Valence: <br />"));
                DropDownList MinValence = new DropDownList { ID = "min_valence", Text = "" };
                MinValence.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MinValence.DataSource = getFloatZeroToOne();
                MinValence.DataTextField = "Value";
                MinValence.DataValueField = "Key";
                MinValence.DataBind();

                collection.Add(MinValence.ID);
                ParamList.Controls.Add(MinValence);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Max. Valence: <br />"));
                DropDownList MaxValence = new DropDownList { ID = "max_valence", Text = "" };
                MaxValence.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                MaxValence.DataSource = getFloatZeroToOne();
                MaxValence.DataTextField = "Value";
                MaxValence.DataValueField = "Key";
                MaxValence.DataBind();

                collection.Add(MaxValence.ID);
                ParamList.Controls.Add(MaxValence);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Target Valence: <br />"));
                DropDownList TargetValence = new DropDownList { ID = "target_valence", Text = "" };
                TargetValence.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                TargetValence.DataSource = getFloatZeroToOne();
                TargetValence.DataTextField = "Value";
                TargetValence.DataValueField = "Key";
                TargetValence.DataBind();

                collection.Add(TargetValence.ID);
                ParamList.Controls.Add(TargetValence);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Spotify Artist ID Seeds: <br />"));
                TextBox SeedArtists = new TextBox { ID = "seed_artists", Text = "" };
                SeedArtists.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                collection.Add(SeedArtists.ID);
                ParamList.Controls.Add(SeedArtists);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Genres Seeds: <br />"));
                TextBox SeedGenres = new TextBox { ID = "seed_genres", Text = "" };
                SeedGenres.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                collection.Add(SeedGenres.ID);
                ParamList.Controls.Add(SeedGenres);

                ParamList.Controls.Add(new LiteralControl("<br /><br /> Spotify Track ID Seeds: <br />"));
                TextBox SeedTracks = new TextBox { ID = "seed_tracks", Text = "" };
                SeedTracks.Attributes["class"] = "col-md-2 col-md-offset-5 col-sm-offset-1 col-xs-10 col-xs-offset-1";

                collection.Add(SeedTracks.ID);
                ParamList.Controls.Add(SeedTracks);

                ParamList.Controls.Add(new LiteralControl("<br /><br />"));
                ParamCollection = collection;

                System.Diagnostics.Debug.WriteLine("Api Call is GetRecommendationsBasedOnSeeds");
            }
            else
            {
                ParamList.Controls.Clear();
                scopeList.ClearSelection();
                apiUrl = "";
                System.Diagnostics.Debug.WriteLine("Api Call is blank");
            }
            Session["selectedApi"] = apiUrl;
        }

        private List<string> ParamCollection
        {
            get
            {
                var collection = ViewState["ParamCollection"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["ParamCollection"] = value; }
        }

        //private List<string> DropDownListIdCollection
        //{
        //    get
        //    {
        //        var collection = ViewState["DropDownListIdCollection"] as List<string>;
        //        return collection ?? new List<string>();
        //    }
        //    set { ViewState["DropDownListIdCollection"] = value; }
        //}

        private Dictionary<string, string> getCountryCode()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("AD", "AD");
            list.Add("AE", "AE");
            list.Add("AF", "AF");
            list.Add("AG", "AG");
            list.Add("AI", "AI");
            list.Add("AL", "AL");
            list.Add("AM", "AM");
            list.Add("AO", "AO");
            list.Add("AQ", "AQ");
            list.Add("AR", "AR");
            list.Add("AS", "AS");
            list.Add("AT", "AT");
            list.Add("AU", "AU");
            list.Add("AW", "AW");
            list.Add("AX", "AX");
            list.Add("AZ", "AZ");   // End of A
            list.Add("BA", "BA");
            list.Add("BB", "BB");
            list.Add("BD", "BD");
            list.Add("BE", "BE");
            list.Add("BF", "BF");
            list.Add("BG", "BG");
            list.Add("BH", "BH");
            list.Add("BI", "BI");
            list.Add("BJ", "BJ");
            list.Add("BL", "BL");
            list.Add("BM", "BM");
            list.Add("BN", "BN");
            list.Add("BO", "BO");
            list.Add("BQ", "BQ");
            list.Add("BR", "BR");
            list.Add("BS", "BS");
            list.Add("BT", "BT");
            list.Add("BV", "BV");
            list.Add("BW", "BW");
            list.Add("BY", "BY");
            list.Add("BZ", "BZ");   // End of B
            list.Add("CA", "CA");
            list.Add("CC", "CC");
            list.Add("CD", "CD");
            list.Add("CF", "CF");
            list.Add("CG", "CG");
            list.Add("CH", "CH");
            list.Add("CI", "CI");
            list.Add("CK", "CK");
            list.Add("CL", "CL");
            list.Add("CM", "CM");
            list.Add("CN", "CN");
            list.Add("CO", "CO");
            list.Add("CR", "CR");
            list.Add("CU", "CU");
            list.Add("CV", "CV");
            list.Add("CW", "CW");
            list.Add("CX", "CX");
            list.Add("CY", "CY");
            list.Add("CZ", "CZ");   // End of C
            list.Add("DE", "DE");
            list.Add("DJ", "DJ");
            list.Add("DK", "DK");
            list.Add("DM", "DM");
            list.Add("DO", "DO");
            list.Add("DZ", "DZ");   // End of D
            list.Add("EC", "EC");
            list.Add("EE", "EE");
            list.Add("EG", "EG");
            list.Add("EH", "EH");
            list.Add("ER", "ER");
            list.Add("ES", "ES");
            list.Add("ET", "ET");   // End of E
            list.Add("FI", "FI");
            list.Add("FJ", "FJ");
            list.Add("FK", "FK");
            list.Add("FM", "FM");
            list.Add("FO", "FO");
            list.Add("FR", "FR");   // End of F
            list.Add("GA", "GA");
            list.Add("GB", "GB");
            list.Add("GD", "GD");
            list.Add("GE", "GE");
            list.Add("GF", "GF");
            list.Add("GG", "GG");
            list.Add("GH", "GH");
            list.Add("GI", "GI");
            list.Add("GL", "GL");
            list.Add("GM", "GM");
            list.Add("GN", "GN");
            list.Add("GP", "GP");
            list.Add("GQ", "GQ");
            list.Add("GR", "GR");
            list.Add("GS", "GS");
            list.Add("GT", "GT");
            list.Add("GU", "GU");
            list.Add("GW", "GW");
            list.Add("GY", "GY");   // End of G
            list.Add("HK", "HK");
            list.Add("HM", "HM");
            list.Add("HN", "HN");
            list.Add("HR", "HR");
            list.Add("HT", "HT");
            list.Add("HU", "HU");   // End of H
            list.Add("ID", "ID");
            list.Add("IE", "IE");
            list.Add("IL", "IL");
            list.Add("IM", "IM");
            list.Add("IN", "IN");
            list.Add("IO", "IO");
            list.Add("IQ", "IQ");
            list.Add("IR", "IR");
            list.Add("IS", "IS");
            list.Add("IT", "IT");   // End of I
            list.Add("JE", "JE");
            list.Add("JM", "JM");
            list.Add("JO", "JO");
            list.Add("JP", "JP");   // End of J
            list.Add("KE", "KE");
            list.Add("KG", "KG");
            list.Add("KH", "KH");
            list.Add("KI", "KI");
            list.Add("KM", "KM");
            list.Add("KN", "KN");
            list.Add("KP", "KP");
            list.Add("KR", "KR");
            list.Add("KW", "KW");
            list.Add("KY", "KY");
            list.Add("KZ", "KZ");   // End of K
            list.Add("LA", "LA");
            list.Add("LB", "LB");
            list.Add("LC", "LC");
            list.Add("LI", "LI");
            list.Add("LK", "LK");
            list.Add("LR", "LR");
            list.Add("LS", "LS");
            list.Add("LT", "LT");
            list.Add("LU", "LU");
            list.Add("LV", "LV");
            list.Add("LY", "LY");   // End of L
            list.Add("MA", "MA");
            list.Add("MC", "MC");
            list.Add("MD", "MD");
            list.Add("ME", "ME");
            list.Add("MF", "MF");
            list.Add("MG", "MG");
            list.Add("MH", "MH");
            list.Add("MK", "MK");
            list.Add("ML", "ML");
            list.Add("MM", "MM");
            list.Add("MN", "MN");
            list.Add("MO", "MO");
            list.Add("MP", "MP");
            list.Add("MQ", "MQ");
            list.Add("MR", "MR");
            list.Add("MS", "MS");
            list.Add("MT", "MT");
            list.Add("MU", "MU");
            list.Add("MV", "MV");
            list.Add("MW", "MW");
            list.Add("MX", "MX");
            list.Add("MY", "MY");
            list.Add("MZ", "MZ");   // End of M
            list.Add("NA", "NA");
            list.Add("NC", "NC");
            list.Add("NE", "NE");
            list.Add("NF", "NF");
            list.Add("NG", "NG");
            list.Add("NI", "NI");
            list.Add("NL", "NL");
            list.Add("NO", "NO");
            list.Add("NP", "NP");
            list.Add("NR", "NR");
            list.Add("NU", "NU");
            list.Add("NZ", "NZ");   // End of N
            list.Add("OM", "OM");   // End of O
            list.Add("PA", "PA");
            list.Add("PE", "PE");
            list.Add("PF", "PF");
            list.Add("PG", "PG");
            list.Add("PH", "PH");
            list.Add("PK", "PK");
            list.Add("PL", "PL");
            list.Add("PM", "PM");
            list.Add("PN", "PN");
            list.Add("PR", "PR");
            list.Add("PS", "PS");
            list.Add("PT", "PT");
            list.Add("PW", "PW");
            list.Add("PY", "PY");   // End of P
            list.Add("QA", "QA");   // End of Q
            list.Add("RE", "RE");
            list.Add("RO", "RO");
            list.Add("RS", "RS");
            list.Add("RU", "RU");
            list.Add("RW", "RW");   // End of R
            list.Add("SA", "SA");
            list.Add("SB", "SB");
            list.Add("SC", "SC");
            list.Add("SD", "SD");
            list.Add("SE", "SE");
            list.Add("SG", "SG");
            list.Add("SH", "SH");
            list.Add("SI", "SI");
            list.Add("SJ", "SJ");
            list.Add("SK", "SK");
            list.Add("SL", "SL");
            list.Add("SM", "SM");
            list.Add("SN", "SN");
            list.Add("SO", "SO");
            list.Add("SR", "SR");
            list.Add("SS", "SS");
            list.Add("ST", "ST");
            list.Add("SV", "SV");
            list.Add("SX", "SX");
            list.Add("SY", "SY");
            list.Add("SZ", "SZ");   // End of S
            list.Add("TC", "TC");
            list.Add("TD", "TD");
            list.Add("TF", "TF");
            list.Add("TG", "TG");
            list.Add("TH", "TH");
            list.Add("TJ", "TC");
            list.Add("TK", "TC");
            list.Add("TL", "TC");
            list.Add("TM", "TC");
            list.Add("TN", "TC");
            list.Add("TO", "TC");
            list.Add("TR", "TC");
            list.Add("TT", "TC");
            list.Add("TV", "TC");
            list.Add("TW", "TC");
            list.Add("TZ", "TC");   // End of T
            list.Add("UA", "UA");
            list.Add("UG", "UG");
            list.Add("UM", "UM");
            list.Add("US", "US");
            list.Add("UY", "UY");
            list.Add("UZ", "UZ");   // End of U
            list.Add("VA", "VA");
            list.Add("VC", "VC");
            list.Add("VE", "VE");
            list.Add("VG", "VG");
            list.Add("VI", "VI");
            list.Add("VN", "VN");
            list.Add("VU", "VU");   // End of V
            list.Add("WF", "WF");
            list.Add("WS", "WS");   // End of W
            list.Add("YE", "YE");
            list.Add("YT", "YT");   // End of Y
            list.Add("ZA", "ZA");
            list.Add("ZM", "ZM");
            list.Add("ZW", "ZW");   // End of Z

            return list;
        }

        private Dictionary<string, string> getAlbumTypeData()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("album", "album");
            list.Add("single", "single");
            list.Add("appears_on", "appears_on");
            list.Add("compilation", "compilation");

            return list;
        }

        private Dictionary<string, string> getLanguageCode()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("aa", "aa");
            list.Add("ab", "ab");
            list.Add("ae", "ae");
            list.Add("af", "af");
            list.Add("ak", "ak");
            list.Add("am", "am");
            list.Add("an", "an");
            list.Add("ar", "ar");
            list.Add("as", "as");
            list.Add("av", "av");
            list.Add("ay", "ay");
            list.Add("az", "az");   // End of A
            list.Add("ba", "ba");
            list.Add("be", "be");
            list.Add("bg", "bg");
            list.Add("bh", "bh");
            list.Add("bi", "bi");
            list.Add("bm", "bm");
            list.Add("bn", "bn");
            list.Add("bo", "bo");
            list.Add("br", "br");
            list.Add("bs", "bs");   // End of B
            list.Add("ca", "ca");
            list.Add("ce", "ce");
            list.Add("ch", "ch");
            list.Add("co", "co");
            list.Add("cr", "cr");
            list.Add("cs", "cs");
            list.Add("cu", "cu");
            list.Add("cv", "cv");
            list.Add("cy", "cy");   // End of c
            list.Add("da", "da");
            list.Add("de", "de");
            list.Add("dv", "dv");
            list.Add("dz", "dz");   // End of d
            list.Add("ee", "ee");
            list.Add("el", "el");
            list.Add("en", "en");
            list.Add("eo", "eo");
            list.Add("es", "es");
            list.Add("et", "et");
            list.Add("eu", "eu");   // End of e
            list.Add("fa", "fa");
            list.Add("ff", "ff");
            list.Add("fi", "fi");
            list.Add("fj", "fj");
            list.Add("fo", "fo");
            list.Add("fr", "fr");
            list.Add("fy", "fy");   // End of f
            list.Add("ga", "ga");
            list.Add("gd", "gd");
            list.Add("gl", "gl");
            list.Add("gn", "gn");
            list.Add("gu", "gu");
            list.Add("gv", "gv");   // End of g
            list.Add("ha", "ha");
            list.Add("he", "he");
            list.Add("hi", "hi");
            list.Add("ho", "ho");
            list.Add("hr", "hr");
            list.Add("ht", "ht");
            list.Add("hu", "hu");
            list.Add("hy", "hy");
            list.Add("hz", "hz");   // End of h
            list.Add("ia", "ia");
            list.Add("id", "id");
            list.Add("ie", "ie");
            list.Add("ig", "ig");
            list.Add("ii", "ii");
            list.Add("ik", "ik");
            list.Add("io", "io");
            list.Add("is", "is");
            list.Add("it", "it");
            list.Add("iu", "iu");   // End of i
            list.Add("ja", "ja");
            list.Add("jv", "jv");   // End of j
            list.Add("ka", "ka");
            list.Add("kg", "kg");
            list.Add("ki", "ki");
            list.Add("kj", "kj");
            list.Add("kk", "kk");
            list.Add("kl", "kl");
            list.Add("km", "km");
            list.Add("kn", "kn");
            list.Add("ko", "ko");
            list.Add("kr", "kr");
            list.Add("ks", "ks");
            list.Add("ku", "ku");
            list.Add("kv", "kv");
            list.Add("kw", "kw");
            list.Add("ky", "ky");   // End of k 
            list.Add("la", "la");
            list.Add("lb", "lb");
            list.Add("lg", "lg");
            list.Add("li", "li");
            list.Add("ln", "ln");
            list.Add("lo", "lo");
            list.Add("lt", "lt");
            list.Add("lu", "lu");
            list.Add("lv", "lv");   // End of l
            list.Add("mg", "mg");
            list.Add("mh", "mh");
            list.Add("mi", "mi");
            list.Add("mk", "mk");
            list.Add("ml", "ml");
            list.Add("mn", "mn");
            list.Add("mr", "mr");
            list.Add("ms", "ms");
            list.Add("mt", "mt");
            list.Add("my", "my");   // End of m
            list.Add("na", "na");
            list.Add("nb", "nb");
            list.Add("nd", "nd");
            list.Add("ne", "ne");
            list.Add("ng", "ng");
            list.Add("nl", "nl");
            list.Add("nn", "nn");
            list.Add("no", "no");
            list.Add("nr", "nr");
            list.Add("nv", "nv");
            list.Add("ny", "ny");   // End of n
            list.Add("oc", "oc");
            list.Add("oj", "oj");
            list.Add("om", "om");
            list.Add("or", "or");
            list.Add("os", "os");   // End of o
            list.Add("pa", "pa");
            list.Add("pi", "pi");
            list.Add("pl", "pl");
            list.Add("ps", "ps");
            list.Add("pt", "pt");   // End of p
            list.Add("qu", "qu");   // End of q
            list.Add("rm", "rm");
            list.Add("rn", "rn");
            list.Add("ro", "ro");
            list.Add("ru", "ru");
            list.Add("rw", "rw");   // End of r
            list.Add("sa", "sa");
            list.Add("sc", "sc");
            list.Add("sd", "sd");
            list.Add("se", "se");
            list.Add("sg", "sg");
            list.Add("si", "si");
            list.Add("sk", "sk");
            list.Add("sl", "sl");
            list.Add("sm", "sm");
            list.Add("sn", "sn");
            list.Add("so", "so");
            list.Add("sq", "sq");
            list.Add("sr", "sr");
            list.Add("ss", "ss");
            list.Add("st", "st");
            list.Add("su", "su");
            list.Add("sv", "sv");
            list.Add("sw", "sw");   // End of s
            list.Add("ta", "ta");
            list.Add("te", "te");
            list.Add("tg", "tg");
            list.Add("th", "th");
            list.Add("ti", "ti");
            list.Add("tk", "tk");
            list.Add("tl", "tl");
            list.Add("tn", "tn");
            list.Add("to", "to");
            list.Add("tr", "tr");
            list.Add("ts", "ts");
            list.Add("tt", "tt");
            list.Add("tw", "tw");
            list.Add("ty", "ty");   // End of t
            list.Add("ug", "ug");
            list.Add("uk", "uk");
            list.Add("ur", "ur");
            list.Add("uz", "uz");   // End of u
            list.Add("ve", "ve");
            list.Add("vi", "vi");
            list.Add("vo", "vo");   // End of v
            list.Add("wa", "wa");
            list.Add("wo", "wo");   // End of w
            list.Add("xh", "xh");   // End of x
            list.Add("yi", "yi");
            list.Add("yo", "yo");   // End of y
            list.Add("za", "za");
            list.Add("zh", "zh");
            list.Add("zu", "zu");   // End of z

            return list;
        }

        private Dictionary<string, string> getItemType()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("artist", "artist");
            list.Add("user", "user");

            return list;
        }

        private Dictionary<string, string> getEntityTypes()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("artists", "artists");
            list.Add("tracks", "tracks");

            return list;
        }

        private Dictionary<string, string> trueOrFalse()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("true", "true");
            list.Add("false", "false");

            return list;
        }

        private Dictionary<string, string> getTimeRange()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("short_term", "short term");
            list.Add("medium_term", "medium term");
            list.Add("long_term", "long term");

            return list;
        }

        private Dictionary<string, string> getTrackAttributesGrade()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("0.0", "0.0");
            list.Add("0.1", "0.1");
            list.Add("0.2", "0.2");
            list.Add("0.3", "0.3");
            list.Add("0.4", "0.4");
            list.Add("0.5", "0.5");
            list.Add("0.6", "0.6");
            list.Add("0.7", "0.7");
            list.Add("0.8", "0.8");
            list.Add("0.9", "0.9");
            list.Add("1.0", "1.0");

            return list;
        }

        private Dictionary<string, string> getKeyTracks()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("0", "0");
            list.Add("1", "1");
            list.Add("2", "2");
            list.Add("3", "3");
            list.Add("4", "4");
            list.Add("5", "5");
            list.Add("6", "6");
            list.Add("7", "7");
            list.Add("8", "8");
            list.Add("9", "9");
            list.Add("10", "10");
            list.Add("11", "11");

            return list;
        }

        private Dictionary<string, string> getDecibels()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            for (int i = -60; i <= 0; i++)
            {
                list.Add(i.ToString(), i.ToString() + " db");
            }

            return list;

        }

        private Dictionary<string, string> getModality()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            list.Add("0", "minor");
            list.Add("1", "major");

            return list;
        }

        private Dictionary<string, string> getMinAndSec()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            for (int i = 0; i <= 60; i++)
            {
                list.Add(i.ToString(), i.ToString());
            }

            return list;
        }

        private Dictionary<string, string> getZeroToHundred()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            for (int i = 0; i <= 100; i++)
            {
                list.Add(i.ToString(), i.ToString());
            }

            return list;
        }

        private Dictionary<string, string> getFloatZeroToOne()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");

            for (int i = 0; i <= 100; i += 1)
            {
                list.Add((i / 100.0f).ToString(), (i / 100.0f).ToString());
            }

            return list;
        }

        private Dictionary<string, string> getFloatZeroToFiveHundred()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");

            for (int i = 0; i <= 500; i ++)
            {
                list.Add(i.ToString() + ".00", i.ToString() + ".00");
            }

            return list;
        }

        private Dictionary<string, string> getHours()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("", "");
            for (int i = 0; i <= 24; i++)
            {
                list.Add(i.ToString(), i.ToString());
            }

            return list;
        }
    }
}