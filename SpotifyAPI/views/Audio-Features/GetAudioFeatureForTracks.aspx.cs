﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Audio_Features
{
    public partial class GetAudioFeatureForTracks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["trackIds"] as string))
                System.Diagnostics.Debug.WriteLine("Track Ids are empty");
            else
                nrParams += (string)(Session["trackIds"]);


            //string url = "https://api.spotify.com/v1/audio-features/?ids=4JpKVNYnVcJ8tuMKjAj50A,2NRANZE9UCmPAS5XVbXL40,24JygzOLM0EmRQeGtFcIcG";
            string url = "https://api.spotify.com/v1/audio-features/?ids=" + nrParams;
            System.Diagnostics.Debug.WriteLine(url);
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);
        }
    }
}