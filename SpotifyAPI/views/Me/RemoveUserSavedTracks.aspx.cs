﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Me
{
    public partial class RemoveUserSavedTracks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["track_ids"] as string))
                System.Diagnostics.Debug.WriteLine("Track Ids are empty");
            else
                nrParams += "?ids=" + (string)(Session["track_ids"]);

            string url = "https://api.spotify.com/v1/me/tracks" + nrParams;
            System.Diagnostics.Debug.WriteLine(url);
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();

            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "DELETE", parameters);
        }
    }
}