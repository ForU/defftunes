﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Me
{
    public partial class GetFollowedArtists : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["type"] as string))
                System.Diagnostics.Debug.WriteLine("Type is empty");
            else
                nrParams += "?type=" + (string)(Session["type"]);

            if (string.IsNullOrEmpty(Session["after"] as string))
                System.Diagnostics.Debug.WriteLine("After is empty");
            else
                nrParams += "&after=" + (string)(Session["after"]);

            if (string.IsNullOrEmpty(Session["limit"] as string))
                System.Diagnostics.Debug.WriteLine("Limit is empty");
            else
                nrParams += "&limit=" + (string)(Session["limit"]);

            //string url = "https://api.spotify.com/v1/me/following?type=artist";
            string url = "https://api.spotify.com/v1/me/following" + nrParams;
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);
        }
    }
}