﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpotifyAPI.views.Callbacks
{
    public partial class Callback : System.Web.UI.Page
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string refresh_token { get; set; }
        public string href { get; set; }    // optional: to call json variable
        public bool stat { get; set; }
        private static string apiUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Callback callback = new Callback();
            //string code = Request.QueryString["code"];
            //string state = Request.QueryString["state"];
            //string grant_type = "authorization_code";
            //string redirect_uri = ((defftunes)this.Master).getRedirectUri();
            //Debug.WriteLine(code);
            //Debug.WriteLine(state);

            //// Generate URL with authorization code
            //string url = "https://accounts.spotify.com/api/token";

            //// get Client ID and Secret
            //string clientId = ((defftunes)this.Master).getClientId();
            //string clientSecret = ((defftunes)this.Master).getClientSecret();

            //const string contentType = "application/x-www-form-urlencoded";
            //string postString = string.Format("grant_type={0}&code={1}&redirect_uri={2}&client_id={3}&client_secret={4}", grant_type, code, redirect_uri, clientId, clientSecret);

            //var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            //webrequest.Method = "POST";
            //webrequest.ContentType = contentType;
            //webrequest.ContentLength = postString.Length;
            //StreamWriter requestWriter = new StreamWriter(webrequest.GetRequestStream());
            //requestWriter.Write(postString);
            //requestWriter.Close();

            ////WebHeaderCollection myWebHeaderCollection = webrequest.Headers;
            ////myWebHeaderCollection.Add("Authorization: Basic " + postAuthorizationCode);

            ////HttpWebResponse myHttpWebResponse = (HttpWebResponse)webrequest.GetResponse();

            ////try
            ////{
            ////    using (WebResponse response = webrequest.GetResponse())
            ////    {
            ////        Debug.WriteLine("Won't get here");
            ////    }
            ////}
            ////catch (WebException a)
            ////{
            ////    using (WebResponse response = a.Response)
            ////    {
            ////        HttpWebResponse httpResponse = (HttpWebResponse)response;
            ////        Debug.WriteLine("Error code: {0}", httpResponse.StatusCode);
            ////        using (Stream data = response.GetResponseStream())
            ////        using (var webreader = new StreamReader(data))
            ////        {
            ////            string text = webreader.ReadToEnd();
            ////            Debug.WriteLine("This is the error: " + text);
            ////        }
            ////    }
            ////}


            ////Stream responseStream = myHttpWebResponse.GetResponseStream();
            ////var reader = new StreamReader(responseStream);
            ////string result = reader.ReadToEnd();
            ////System.Diagnostics.Debug.WriteLine("Unparsed json response from server: " + result);

            ////Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);
            //////System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + jsonResponse.access_token);
            //////System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + jsonResponse.token_type);
            //////System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + jsonResponse.expires_in);
            //////System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + jsonResponse.refresh_token);

            ////// Store json responses in session variables
            ////Session["accessTokenVar"] = jsonResponse.access_token;
            ////Session["tokenTypeVar"] = jsonResponse.token_type;
            ////Session["expiresInVar"] = jsonResponse.expires_in;
            ////Session["refreshTokenVar"] = jsonResponse.refresh_token;

            //////System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            //////System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            //////System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            //////System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));

            //////string url1 = "https://api.spotify.com/v1/me";
            ////////const string contentType1 = "application/x-www-form-urlencoded";
            //////string postString1 = string.Format("grant_type={0}&code={1}&redirect_uri={2}&client_id={3}&client_secret={4}", grant_type, code, redirect_uri, clientId, clientSecret);

            //////var webrequest1 = (HttpWebRequest)WebRequest.CreateHttp(url1);
            ////////webrequest1.Headers("Authorization: Bearer " + jsonResponse.access_token);
            //////WebHeaderCollection myWebHeaderCollection1 = webrequest1.Headers;
            //////myWebHeaderCollection1.Add("Authorization: Bearer " + jsonResponse.access_token);
            //////webrequest1.Method = "GET";

            //////var webresponse1 = (HttpWebResponse)webrequest1.GetResponse();
            //////Stream responseStream1 = webresponse1.GetResponseStream();
            //////var reader1 = new StreamReader(responseStream1);
            //////string result1 = reader1.ReadToEnd();
            //////System.Diagnostics.Debug.WriteLine("The result from profile request: " + result1);

            ////// API Methods
            //////Server.Transfer("GetCurrentUserProfile.aspx", true);
            //////Response.Redirect("GetCurrentUserProfile.aspx");
            //////Response.Redirect("../Albums/GetAlbum.aspx");
            //////Response.Redirect("../Albums/GetAlbums.aspx");
            ////Response.Redirect("../Albums/GetAlbumTracks.aspx");
            //////Response.Redirect("GetArtists.aspx");
            //////Response.Redirect("GetArtistAlbums.aspx");
            //////Response.Redirect("GetArtistTopTracks.aspx");
            //////Response.Redirect("GetArtistRelatedArtists.aspx");
            //////Response.Redirect("GetAudioFeatures.aspx");
            //////Response.Redirect("GetAudioFeaturesForTracks.aspx");
            //////Response.Redirect("GetListOfFeaturedPlaylist.aspx");
            //////Response.Redirect("../Browse/NewReleases.aspx");

            //try
            //{
            //    using (var response = webrequest.GetResponse() as HttpWebResponse)
            //    {
            //        if (webrequest.HaveResponse && response != null)
            //        {
            //            using (var reader = new StreamReader(response.GetResponseStream()))
            //            {
            //                string result = reader.ReadToEnd();
            //                Debug.WriteLine("Results: " + result);
            //                System.Diagnostics.Debug.WriteLine("Unparsed json response from server: " + result);

            //                Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);
            //                Session["accessTokenVar"] = jsonResponse.access_token;
            //                Session["tokenTypeVar"] = jsonResponse.token_type;
            //                Session["expiresInVar"] = jsonResponse.expires_in;
            //                Session["refreshTokenVar"] = jsonResponse.refresh_token;

            //                // API Methods
            //                //Server.Transfer("GetCurrentUserProfile.aspx", true);
            //                //Response.Redirect("GetCurrentUserProfile.aspx");
            //                //Response.Redirect("../Albums/GetAlbum.aspx");
            //                //Response.Redirect("../Albums/GetAlbums.aspx");
            //                //Response.Redirect("../Albums/GetAlbumTracks.aspx");
            //                //Response.Redirect("../Artists/GetArtist.aspx");
            //                //Response.Redirect("../Artists/GetArtists.aspx");
            //                //Response.Redirect("../Artists/GetArtistAlbums.aspx");
            //                //Response.Redirect("../Artists/GetArtistTopTracks.aspx");
            //                //Response.Redirect("../Artists/GetArtistRelatedArtists.aspx");
            //                //Response.Redirect("../Audio-Features/GetAudioFeatureForTrack.aspx");
            //                //Response.Redirect("../Audio-Features/GetAudioFeatureForTracks.aspx");
            //                Response.Redirect("../Browse/FeaturedPlaylists.aspx");
            //                //Response.Redirect("../Browse/NewReleases.aspx");

            //                //if()
            //                //{

            //                //}
            //            }
            //        }
            //    }
            //}
            //catch (WebException wex)
            //{
            //    if (wex.Response != null)
            //    {
            //        using (var errorResponse = (HttpWebResponse)wex.Response)
            //        {
            //            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
            //            {
            //                string error = reader.ReadToEnd();
            //                Debug.WriteLine("Details of the error: " + error);
            //            }
            //        }
            //    }
            //}

            //Debug.WriteLine("The Selected Item is: " + ApiList.SelectedItem.Value);
            deployApi((string)Session["selectedApi"]);
        }

        protected void deployApi(string apiDestination)
        {
            Callback callback = new Callback();
            string code = HttpContext.Current.Request.QueryString["code"];
            string state = HttpContext.Current.Request.QueryString["state"];
            string grant_type = "authorization_code";
            string redirect_uri = ((defftunes)this.Master).getRedirectUri();
            Debug.WriteLine(code);
            Debug.WriteLine(state);

            // Generate URL with authorization code
            string url = "https://accounts.spotify.com/api/token";

            // get Client ID and Secret
            string clientId = ((defftunes)this.Master).getClientId();
            string clientSecret = ((defftunes)this.Master).getClientSecret();

            const string contentType = "application/x-www-form-urlencoded";
            string postString = string.Format("grant_type={0}&code={1}&redirect_uri={2}&client_id={3}&client_secret={4}", grant_type, code, redirect_uri, clientId, clientSecret);

            var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            webrequest.Method = "POST";
            webrequest.ContentType = contentType;
            webrequest.ContentLength = postString.Length;
            StreamWriter requestWriter = new StreamWriter(webrequest.GetRequestStream());
            requestWriter.Write(postString);
            requestWriter.Close();

            try
            {
                using (var response = webrequest.GetResponse() as HttpWebResponse)
                {
                    if (webrequest.HaveResponse && response != null)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            string result = reader.ReadToEnd();
                            Debug.WriteLine("Results: " + result);
                            System.Diagnostics.Debug.WriteLine("Unparsed json response from server: " + result);

                            Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);
                            Session["accessTokenVar"] = jsonResponse.access_token;
                            Session["tokenTypeVar"] = jsonResponse.token_type;
                            Session["expiresInVar"] = jsonResponse.expires_in;
                            Session["refreshTokenVar"] = jsonResponse.refresh_token;

                            // API Methods
                            //Server.Transfer("GetCurrentUserProfile.aspx", true);
                            //Response.Redirect("GetCurrentUserProfile.aspx");
                            //Response.Redirect("../Albums/GetAlbum.aspx");
                            //Response.Redirect("../Albums/GetAlbums.aspx");
                            //Response.Redirect("../Albums/GetAlbumTracks.aspx");
                            //Response.Redirect("../Artists/GetArtist.aspx");
                            //Response.Redirect("../Artists/GetArtists.aspx");
                            //Response.Redirect("../Artists/GetArtistAlbums.aspx");
                            //Response.Redirect("../Artists/GetArtistTopTracks.aspx");
                            //Response.Redirect("../Artists/GetArtistRelatedArtists.aspx");
                            //Response.Redirect("../Audio-Features/GetAudioFeatureForTrack.aspx");
                            //Response.Redirect("../Audio-Features/GetAudioFeatureForTracks.aspx");
                            //Response.Redirect("../Browse/FeaturedPlaylists.aspx");
                            //Response.Redirect("../Browse/NewReleases.aspx");

                            Response.Redirect(apiDestination);
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            Debug.WriteLine("Details of the error: " + error);
                        }
                    }
                }
            }
        }

        protected void SelectAPI_Click(object sender, EventArgs e)
        {
            //if(ApiList.SelectedItem.Value == "")
            //{
            //    SelectApiButton.Enabled = false;
            //}
            //else
            //{ 
            //    deployApi(apiUrl);
            //}
            Debug.WriteLine("The session api call is: " + (string)Session["selectedApi"]);
            deployApi((string)Session["selectedApi"]);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        //public string ProcessApi(string url, string tokenVar, string method, List<string> parameters = null)
        public string ProcessApi(string url, string tokenVar, string method, List<string> parameters = null)
        {
            var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            WebHeaderCollection myWebHeaderCollection = webrequest.Headers;
            myWebHeaderCollection.Add("Authorization: Bearer " + tokenVar);
            //webrequest.Method = "GET";
            webrequest.Method = method;
            string innerHtmlText = "";

            //if (parameters != null)
            if (parameters.Count != 0)
            {
                Debug.WriteLine("parameters are not null");
                //string json = "{\"public\": false}";
                Debug.WriteLine(parameters.Count);
                using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
                {
                    //streamWriter.Write(i);
                    foreach (var i in parameters)
                    {
                        Debug.WriteLine(i);
                        streamWriter.Write(i);
                    }
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }

            try
            {
                using (var response = webrequest.GetResponse() as HttpWebResponse)
                {
                    if (webrequest.HaveResponse && response != null)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            string result = reader.ReadToEnd();
                            if (result == "")
                            {
                                //showResult.InnerHtml = "<pre><code>Server returns no responses.</code></pre>";
                                innerHtmlText = "<pre><code>Server returns no responses.</code></pre>";
                                return innerHtmlText;
                            }
                            else
                            {
                                Debug.WriteLine("Results: " + result);
                                System.Diagnostics.Debug.WriteLine("Unparsed json response from server: " + result);

                                JavaScriptSerializer ser = new JavaScriptSerializer();
                                //Callback jsonResponse = ser.Deserialize<Callback>(result);

                                try
                                {
                                    Callback jsonResponse = ser.Deserialize<Callback>(result);
                                    System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
                                    System.Diagnostics.Debug.WriteLine("The result from profile request: " + jsonResponse.href);
                                    System.Diagnostics.Debug.WriteLine("Its a json object");
                                    innerHtmlText = "<pre><code>" + result + "</code></pre>";
                                    return innerHtmlText;
                                }
                                catch
                                {
                                    // For json arrays with single statements: [true] or [false]
                                    System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
                                    System.Diagnostics.Debug.WriteLine("Its an json array 1");
                                    innerHtmlText = "<pre><code>" + result + "</code></pre>";
                                    return innerHtmlText;
                                }
                                //catch
                                //{
                                //    //List <Callback> jsonResponse = ser.Deserialize<List<Callback>>(result);
                                //    System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
                                //    System.Diagnostics.Debug.WriteLine("Its an json array 2");
                                //    innerHtmlText = "<pre><code>" + result + "</code></pre>";
                                //    return innerHtmlText;
                                //}

                                //System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
                                //System.Diagnostics.Debug.WriteLine("The result from profile request: " + jsonResponse.href);
                                // //showResult.InnerHtml = "<pre><code>" + result + "</code></pre>";

                                //innerHtmlText = "<pre><code>" + result + "</code></pre>";
                                //return innerHtmlText;
                            }

                        }
                    }
                }
            }
            catch (WebException wex)
            {
                Debug.WriteLine("This program is expected to throw WebException on successful run." +
                                    "\n\nException Message :" + wex.Message);
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ((HttpWebResponse)wex.Response);
                    Debug.WriteLine("Status Code : {0}", response.StatusCode);
                    Debug.WriteLine("Status Description : {0}", response.StatusDescription);

                    try
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(stream))
                            {
                                var text = reader.ReadToEnd();
                                if (text != "")
                                {
                                    Debug.WriteLine("Full error Description: " + text);
                                    //showResult.InnerHtml = "<pre><code>" + text + "</code></pre>";
                                    innerHtmlText = "<pre><code>" + text + "</code></pre>";
                                    System.Diagnostics.Debug.WriteLine("This is the result: " + innerHtmlText);
                                    return innerHtmlText;
                                }
                                else
                                {
                                    Debug.WriteLine("Full error Description: " + response.StatusDescription);
                                    //showResult.InnerHtml = "<pre><code>The server returns \"" + response.StatusDescription + "\"There might be an issue with the query's syntax.</code></pre>";
                                    innerHtmlText = "<pre><code>The server returns \"" + response.StatusDescription + "\". There might be an issue with the query's syntax OR scope is required.</code></pre>";
                                    System.Diagnostics.Debug.WriteLine("This is the result: " + innerHtmlText);
                                    return innerHtmlText;
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        // Oh, well, we tried
                    }
                }
            }
            catch (Exception wex)
            {
                Debug.WriteLine(wex.Message);
            }
            return "";
        }
    }
}