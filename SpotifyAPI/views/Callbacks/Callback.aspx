﻿<%@ Page Title="" Language="C#" MasterPageFile="~/views/defftunes.Master" AutoEventWireup="true" CodeBehind="Callback.aspx.cs" Inherits="SpotifyAPI.views.Callbacks.Callback" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form class="text-center"  runat="server">
        <div class="form-group">
            <%--<asp:DropDownList id="ApiList" class="ApiList" AutoPostBack="True" OnSelectedIndexChanged="Selection_Change" runat="server">
                <asp:ListItem Selected="True" Value="GetAlbumId"> Get Album Id </asp:ListItem>
                <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                <asp:ListItem Value="GetAlbumId"> Get Album Id </asp:ListItem>
                <asp:ListItem Value="GetAlbumIds"> Get Albums Ids </asp:ListItem>
                <asp:ListItem Value="GetAlbumTracks"> Get Album Tracks </asp:ListItem>
                <asp:ListItem Value="GetArtist"> Get An Artist </asp:ListItem>
                <asp:ListItem Value="GetArtists"> Get Several Artists </asp:ListItem>
                <asp:ListItem Value="GetArtistAlbums"> Get Artist's Albums </asp:ListItem>
                <asp:ListItem Value="GetArtistTopTracks"> Get Artist's Top Tracks </asp:ListItem>
                <asp:ListItem Value="GetArtistRelatedArtists"> Get Artist's Related Artists </asp:ListItem>
                <asp:ListItem Value="GetAudioFeatureForTrack"> Get Audio Features For A Track </asp:ListItem>
                <asp:ListItem Value="GetAudioFeatureForTracks"> Get Audio Features For Several Tracks </asp:ListItem>
                <asp:ListItem Value="FeaturedPlaylists"> Featured Playlists </asp:ListItem>
                <asp:ListItem Value="NewReleases"> New Releases </asp:ListItem>
                <asp:ListItem Value="GetCategory"> Get Category </asp:ListItem>
                <asp:ListItem Value="GetCategories"> Get Categories </asp:ListItem>
                <asp:ListItem Value="GetCategoryPlaylists"> Get Category Playlists </asp:ListItem>
                <asp:ListItem Value="GetMe"> Get Me </asp:ListItem>
                <asp:ListItem Value="GetFollowedArtists"> Get Followed Artists </asp:ListItem>
            </asp:DropDownList>--%>
            </div>
            <asp:Button id="SelectApiButton" runat="server" type="button" class="SelectApiButton btn btn-success" onclick="SelectAPI_Click" text="Select"></asp:Button>
        <%--</div>--%>
    </form>
</asp:Content>
