﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Browse
{
    public partial class GetCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["categoryId"] as string))
                System.Diagnostics.Debug.WriteLine("Category Id is empty");
            else
                nrParams += (string)(Session["categoryId"]) + "?";

            if (string.IsNullOrEmpty(Session["locale_1"] as string))
                System.Diagnostics.Debug.WriteLine("Locale_1 is empty");
            else
                nrParams += "&locale=" + (string)(Session["locale_1"]);

            if (string.IsNullOrEmpty(Session["locale_2"] as string))
                System.Diagnostics.Debug.WriteLine("Locale_2 is empty");
            else
                nrParams += "_" + (string)(Session["locale_2"]);

            if (string.IsNullOrEmpty(Session["country"] as string))
                System.Diagnostics.Debug.WriteLine("Country is empty");
            else
                nrParams += "&country=" + (string)(Session["country"]);

            //string url = "https://api.spotify.com/v1/browse/categories/party";
            string url = "https://api.spotify.com/v1/browse/categories/" + nrParams;
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);
        }
    }
}