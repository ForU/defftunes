﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Browse
{
    public partial class NewReleases : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");


            string nrParams = "";

            if (string.IsNullOrEmpty(Session["country"] as string))
                System.Diagnostics.Debug.WriteLine("Country is empty");
            else
                nrParams += "?country=" + (string)(Session["country"]) + "";
            if (string.IsNullOrEmpty(Session["limit"] as string))
                System.Diagnostics.Debug.WriteLine("Limit is empty");
            else
                nrParams += "&limit=" + (string)(Session["limit"]) + "";
            if (string.IsNullOrEmpty(Session["offset"] as string))
                System.Diagnostics.Debug.WriteLine("Offset is empty");
            else
                nrParams += "&offset=" + (string)(Session["offset"]) + "";

            //string url = "https://api.spotify.com/v1/browse/new-releases?country=SE";
            string url = "https://api.spotify.com/v1/browse/new-releases" + nrParams;
            System.Diagnostics.Debug.WriteLine(url);
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);

            System.Diagnostics.Debug.WriteLine("hello to you again");
            System.Diagnostics.Debug.WriteLine((string)(Session["country"]));
            System.Diagnostics.Debug.WriteLine((string)(Session["limit"]));
            System.Diagnostics.Debug.WriteLine((string)(Session["offset"]));

            //System.Text.StringBuilder displayValues =
            //new System.Text.StringBuilder();
            //System.Collections.Specialized.NameValueCollection
            //    postedValues = Request.Form;
            //String nextKey;
            //for (int i = 0; i < postedValues.AllKeys.Length; i++)
            //{
            //    nextKey = postedValues.AllKeys[i];
            //    if (nextKey.Substring(0, 2) != "__")
            //    {
            //        displayValues.Append("<br>");
            //        displayValues.Append(nextKey);
            //        displayValues.Append(" = ");
            //        displayValues.Append(postedValues[i]);
            //    }
            //}
            //System.Diagnostics.Debug.WriteLine(displayValues.ToString());
        }
    }
}