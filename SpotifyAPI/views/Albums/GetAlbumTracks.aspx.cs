﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Albums
{
    public partial class GetAlbumTracks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["albumId"] as string))
                System.Diagnostics.Debug.WriteLine("Album Id is empty");
            else
                nrParams += (string)(Session["albumId"]);

            if (string.IsNullOrEmpty(Session["offset"] as string))
                System.Diagnostics.Debug.WriteLine("Offset is empty");
            else
                nrParams += "/tracks?offset=" + (string)(Session["offset"]) + "";

            if (string.IsNullOrEmpty(Session["limit"] as string))
                System.Diagnostics.Debug.WriteLine("Limit is empty");
            else
                nrParams += "&limit=" + (string)(Session["limit"]) + "";

            if (string.IsNullOrEmpty(Session["market"] as string))
                System.Diagnostics.Debug.WriteLine("Market is empty");
            else
                nrParams += "&market=" + (string)(Session["market"]) + "";

            //string url = "https://api.spotify.com/v1/albums/4aawyAB9vmqN3uQ7FjRGTy/tracks?offset=0&limit=2";
            string url = "https://api.spotify.com/v1/albums/" + nrParams;
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);

        }
    }
}