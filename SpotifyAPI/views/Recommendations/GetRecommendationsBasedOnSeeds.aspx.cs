﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using SpotifyAPI.views.Callbacks;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpotifyAPI.views.Recommendations
{
    public partial class GetRecommendationsBasedOnSeeds : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string nrParams = "";

            if (string.IsNullOrEmpty(Session["min_acousticness"] as string))
                System.Diagnostics.Debug.WriteLine("Min Acoustiness is empty");
            else
                nrParams += "min_acousticness=" + (string)(Session["min_acousticness"]) + "&";

            if (string.IsNullOrEmpty(Session["max_acousticness"] as string))
                System.Diagnostics.Debug.WriteLine("Max Acousticness is empty");
            else
                nrParams += "max_acousticness=" + (string)(Session["max_acousticness"]) + "&";

            if (string.IsNullOrEmpty(Session["target_acousticness"] as string))
                System.Diagnostics.Debug.WriteLine("Target Acousticness is empty");
            else
                nrParams += "target_acousticness=" + (string)(Session["target_acousticness"]) + "&";

            if (string.IsNullOrEmpty(Session["min_danceability"] as string))
                System.Diagnostics.Debug.WriteLine("Min Danceability is empty");
            else
                nrParams += "min_danceability=" + (string)(Session["min_danceability"]) + "&";

            if (string.IsNullOrEmpty(Session["max_danceability"] as string))
                System.Diagnostics.Debug.WriteLine("Max Danceability is empty");
            else
                nrParams += "max_danceability=" + (string)(Session["max_danceability"]) + "&";

            if (string.IsNullOrEmpty(Session["target_danceability"] as string))
                System.Diagnostics.Debug.WriteLine("Target Danceability is empty");
            else
                nrParams += "target_danceability=" + (string)(Session["target_danceability"]) + "&";

            if (string.IsNullOrEmpty(Session["min_duration"] as string))
                System.Diagnostics.Debug.WriteLine("Min Duration is empty");
            else
                nrParams += "min_duration=" + (string)(Session["min_duration"]) + "&";

            if (string.IsNullOrEmpty(Session["max_duration"] as string))
                System.Diagnostics.Debug.WriteLine("Max Duration is empty");
            else
                nrParams += "max_duration=" + (string)(Session["max_duration"]) + "&";

            if (string.IsNullOrEmpty(Session["target_duration"] as string))
                System.Diagnostics.Debug.WriteLine("Target Duration is empty");
            else
                nrParams += "target_duration=" + (string)(Session["target_duration"]) + "&";

            if (string.IsNullOrEmpty(Session["min_energy"] as string))
                System.Diagnostics.Debug.WriteLine("Min Energy is empty");
            else
                nrParams += "min_energy=" + (string)(Session["min_energy"]) + "&";

            if (string.IsNullOrEmpty(Session["max_energy"] as string))
                System.Diagnostics.Debug.WriteLine("Max Energy is empty");
            else
                nrParams += "max_energy=" + (string)(Session["max_energy"]) + "&";

            if (string.IsNullOrEmpty(Session["target_energy"] as string))
                System.Diagnostics.Debug.WriteLine("Target Energy is empty");
            else
                nrParams += "target_energy=" + (string)(Session["target_energy"]) + "&";

            if (string.IsNullOrEmpty(Session["min_instrumentalness"] as string))
                System.Diagnostics.Debug.WriteLine("Min Instrumentalness is empty");
            else
                nrParams += "min_instrumentalness=" + (string)(Session["min_instrumentalness"]) + "&";

            if (string.IsNullOrEmpty(Session["max_instrumentalness"] as string))
                System.Diagnostics.Debug.WriteLine("Max Instrumentalness is empty");
            else
                nrParams += "max_instrumentalness=" + (string)(Session["max_instrumentalness"]) + "&";

            if (string.IsNullOrEmpty(Session["target_instrumentalness"] as string))
                System.Diagnostics.Debug.WriteLine("Target Instrumentalness is empty");
            else
                nrParams += "target_instrumentalness=" + (string)(Session["target_instrumentalness"]) + "&";

            if (string.IsNullOrEmpty(Session["min_key"] as string))
                System.Diagnostics.Debug.WriteLine("Min Key is empty");
            else
                nrParams += "min_key=" + (string)(Session["min_key"]) + "&";

            if (string.IsNullOrEmpty(Session["max_key"] as string))
                System.Diagnostics.Debug.WriteLine("Max Key is empty");
            else
                nrParams += "max_key=" + (string)(Session["max_key"]) + "&";

            if (string.IsNullOrEmpty(Session["target_key"] as string))
                System.Diagnostics.Debug.WriteLine("Target key is empty");

            if (string.IsNullOrEmpty(Session["min_liveness"] as string))
                System.Diagnostics.Debug.WriteLine("Min Liveness is empty");
            else
                nrParams += "min_liveness=" + (string)(Session["min_liveness"]) + "&";

            if (string.IsNullOrEmpty(Session["max_liveness"] as string))
                System.Diagnostics.Debug.WriteLine("Max Liveness is empty");
            else
                nrParams += "max_liveness=" + (string)(Session["max_liveness"]) + "&";

            if (string.IsNullOrEmpty(Session["target_liveness"] as string))
                System.Diagnostics.Debug.WriteLine("Target Liveness is empty");
            else
                nrParams += "target_liveness=" + (string)(Session["target_livenessy"]) + "&";

            if (string.IsNullOrEmpty(Session["seed_tracks"] as string))
                System.Diagnostics.Debug.WriteLine("Spotify Track ID Seeds is empty");
            else
                nrParams += "seed_tracks=" + (string)(Session["seed_tracks"]) + "&";

            if (string.IsNullOrEmpty(Session["seed_artists"] as string))
                System.Diagnostics.Debug.WriteLine("Spotify Artist ID Seeds is empty");
            else
                nrParams += "seed_artists=" + (string)(Session["seed_artists"]) + "&";

            if (string.IsNullOrEmpty(Session["seed_genres"] as string))
                System.Diagnostics.Debug.WriteLine("Genres Seeds is empty");
            else
                nrParams += "seed_genres=" + (string)(Session["seed_genres"]) + "&";

            if (string.IsNullOrEmpty(Session["limit"] as string))
                System.Diagnostics.Debug.WriteLine("Limit is empty");
            else
                nrParams += "limit=" + (string)(Session["limit"]) + "&";

            if (string.IsNullOrEmpty(Session["market"] as string))
                System.Diagnostics.Debug.WriteLine("Market is empty");
            else
                nrParams += "market=" + (string)(Session["market"]);

            string url = "https://api.spotify.com/v1/recommendations?" + nrParams;
            System.Diagnostics.Debug.WriteLine(url);
            string accessTokenVar = (string)(Session["accessTokenVar"]);

            Callback classResponse = new Callback();
            List<string> parameters = new List<string>();
            parameters.Clear();

            showResult.InnerHtml = classResponse.ProcessApi(url, accessTokenVar, "GET", parameters);
        }
    }
}