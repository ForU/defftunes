﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpotifyAPI
{
    public partial class GetArtistRelatedArtists : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string url = "https://api.spotify.com/v1/artists/43ZHCT0cAZBISjO8DG9PnE/related-artists";

            var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            WebHeaderCollection myWebHeaderCollection = webrequest.Headers;
            myWebHeaderCollection.Add("Authorization: Bearer " + (string)(Session["accessTokenVar"]));
            webrequest.Method = "GET";

            var webresponse = (HttpWebResponse)webrequest.GetResponse();
            Stream responseStream = webresponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            string result = reader.ReadToEnd();
            //Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);

            //var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Callback>(result);
            //Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(obj));

            // Deserializing json arrays
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Callback jsonResponse = ser.Deserialize<Callback>(result);

            System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
            System.Diagnostics.Debug.WriteLine("The result from profile request: " + jsonResponse.href);
            showResult.InnerHtml = "<pre><code>" + result + "</code></pre>";
        }
    }
}