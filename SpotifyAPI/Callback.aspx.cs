﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpotifyAPI
{
    public partial class Callback : System.Web.UI.Page
    {

        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string refresh_token { get; set; }
        public string href { get; set; }    // optional: to call json variable

        protected void Page_Load(object sender, EventArgs e)
        {
            Callback callback = new Callback();
            string code = Request.QueryString["code"];
            string state = Request.QueryString["state"];
            string grant_type = "authorization_code";
            string redirect_uri = ((Site1)this.Master).getRedirectUri();
            Debug.WriteLine(code);
            Debug.WriteLine(state);

            // Generate URL with authorization code
            string url = "https://accounts.spotify.com/api/token";

            // get Client ID and Secret
            string clientId = ((Site1)this.Master).getClientId();
            string clientSecret = ((Site1)this.Master).getClientSecret();

            const string contentType = "application/x-www-form-urlencoded";
            string postString = string.Format("grant_type={0}&code={1}&redirect_uri={2}&client_id={3}&client_secret={4}", grant_type, code, redirect_uri, clientId, clientSecret);

            var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            webrequest.Method = "POST";
            webrequest.ContentType = contentType;
            webrequest.ContentLength = postString.Length;
            StreamWriter requestWriter = new StreamWriter(webrequest.GetRequestStream());
            requestWriter.Write(postString);
            requestWriter.Close();

            //WebHeaderCollection myWebHeaderCollection = webrequest.Headers;
            //myWebHeaderCollection.Add("Authorization: Basic " + postAuthorizationCode);

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)webrequest.GetResponse();

            Stream responseStream = myHttpWebResponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            string result = reader.ReadToEnd();
            System.Diagnostics.Debug.WriteLine("Unparsed json response from server: " + result);

            Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);
            //System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + jsonResponse.access_token);
            //System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + jsonResponse.token_type);
            //System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + jsonResponse.expires_in);
            //System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + jsonResponse.refresh_token);

            // Store json responses in session variables
            Session["accessTokenVar"] = jsonResponse.access_token;
            Session["tokenTypeVar"] = jsonResponse.token_type;
            Session["expiresInVar"] = jsonResponse.expires_in;
            Session["refreshTokenVar"] = jsonResponse.refresh_token;

            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));

            //string url1 = "https://api.spotify.com/v1/me";
            ////const string contentType1 = "application/x-www-form-urlencoded";
            //string postString1 = string.Format("grant_type={0}&code={1}&redirect_uri={2}&client_id={3}&client_secret={4}", grant_type, code, redirect_uri, clientId, clientSecret);

            //var webrequest1 = (HttpWebRequest)WebRequest.CreateHttp(url1);
            ////webrequest1.Headers("Authorization: Bearer " + jsonResponse.access_token);
            //WebHeaderCollection myWebHeaderCollection1 = webrequest1.Headers;
            //myWebHeaderCollection1.Add("Authorization: Bearer " + jsonResponse.access_token);
            //webrequest1.Method = "GET";

            //var webresponse1 = (HttpWebResponse)webrequest1.GetResponse();
            //Stream responseStream1 = webresponse1.GetResponseStream();
            //var reader1 = new StreamReader(responseStream1);
            //string result1 = reader1.ReadToEnd();
            //System.Diagnostics.Debug.WriteLine("The result from profile request: " + result1);

            // API Methods
            //Server.Transfer("GetCurrentUserProfile.aspx", true);
            //Response.Redirect("GetCurrentUserProfile.aspx");
            //Response.Redirect("GetAlbum.aspx");
            //Response.Redirect("GetAlbums.aspx");
            //Response.Redirect("GetAlbumTracks.aspx");
            //Response.Redirect("GetArtists.aspx");
            //Response.Redirect("GetArtistAlbums.aspx");
            //Response.Redirect("GetArtistTopTracks.aspx");
            //Response.Redirect("GetArtistRelatedArtists.aspx");
            //Response.Redirect("GetAudioFeatures.aspx");
            //Response.Redirect("GetAudioFeaturesForTracks.aspx");
            //Response.Redirect("GetListOfFeaturedPlaylist.aspx");
            Response.Redirect("NewReleases.aspx");
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}