﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpotifyAPI
{
    public partial class GetAlbum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("The following logs below are from the GetCurrentUserProfile page.");
            System.Diagnostics.Debug.WriteLine("Response from server - access_token: " + (string)(Session["accessTokenVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - token_type: " + (string)(Session["tokenTypeVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - expires_in: " + (string)(Session["expiresInVar"]));
            System.Diagnostics.Debug.WriteLine("Response from server - refresh_token: " + (string)(Session["refreshTokenVar"]));
            System.Diagnostics.Debug.WriteLine("hello to you too");

            string url = "https://api.spotify.com/v1/albums/0sNOF9WDwhWunNAHPD3Baj";

            var webrequest = (HttpWebRequest)WebRequest.CreateHttp(url);
            WebHeaderCollection myWebHeaderCollection = webrequest.Headers;
            myWebHeaderCollection.Add("Authorization: Bearer " + (string)(Session["accessTokenVar"]));
            webrequest.Method = "GET";

            var webresponse = (HttpWebResponse)webrequest.GetResponse();
            Stream responseStream = webresponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            string result = reader.ReadToEnd();
            System.Diagnostics.Debug.WriteLine("Unformatted result: " + result);

            // Deserializing json object
            Callback jsonResponse = JsonConvert.DeserializeObject<Callback>(result);
            System.Diagnostics.Debug.WriteLine("The result from profile request: " + result);
            showResult.InnerHtml = "<pre><code>" + result + "</code></pre>";
        }
    }
}